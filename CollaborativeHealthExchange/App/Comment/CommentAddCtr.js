﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('CommentAddCtr', ['CommentService', '$state', 'SweetAlert', CommentAddCtr]);

    function CommentAddCtr(CommentService, $state, SweetAlert) {
        var vm = this;
        vm.save = save;
        vm.cancel = cancel;


        activate();

        // ============ Implementation ============
        function activate() {
            vm.Body = '';
            vm.ErrorMessage = '';
            vm.ForumPostID = $state.params.id;
        }

        function save() {
            CommentService.add(vm).then(function (comment) {
                comment.isOwner = true;
                activate();
            }, function (failedMessage) {
                vm.ErrorMessage = failedMessage;
            });
        }

        function cancel() {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your progress will be lost",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, continue!"
            },
            function (isConfirm) {
                if (isConfirm) {
                    activate();
                }
            });
        }

        
        
    }
})();
