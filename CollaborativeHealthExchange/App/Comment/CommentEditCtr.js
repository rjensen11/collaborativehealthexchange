﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('CommentEditCtr', ['CommentService', '$state', 'SweetAlert', CommentEditCtr]);

    function CommentEditCtr(CommentService, $state, SweetAlert) {
        var vm = this;
        vm.save = save;
        vm.cancel = cancel;


        activate();

        // ============ Implementation ============
        function activate() {
            vm.ErrorMessage = '';
            vm.ForumPostID = $state.params.id;
            CommentService.getComment($state.params.commentID).then(function (comment) {
                vm = angular.extend(vm, comment);
            }, function (errorMessage) {
                vm.ErrorMessage = 'There was an error adding the comment';
            });
        }

        function save() {
            CommentService.edit(vm).then(function () {
                $state.go('forum.details', { id: vm.ForumPostID });
            }, function (failedMessage) {
                vm.ErrorMessage = failedMessage;
            });
        }

        function cancel() {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your progress will be lost",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, continue!"
            },
            function (isConfirm) {
                if (isConfirm) {
                    $state.go('forum.details', { id: vm.ForumPostID });
                }
            });
        }

        
        
    }
})();
