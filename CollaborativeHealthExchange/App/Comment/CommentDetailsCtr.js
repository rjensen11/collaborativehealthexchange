﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('CommentDetailsCtr', ['CommentService', 'AccountService', '$state', '$sce', 'SweetAlert', CommentDetailsCtr]);

    function CommentDetailsCtr(CommentService, AccountService, $state, $sce, SweetAlert) {
        var vm = this;
        vm.edit = edit;
        vm.remove = remove;
        vm.comments = [];

        vm.castVote = castVote;
        vm.removeVote = removeVote;
        vm.voteFilter = voteFilter;

        activate();

        // ============ Implementation ============
        function activate() { 
            CommentService.getCommentsForForum($state.params.id).then(function (comments) {
                vm.comments = comments;

                var loggedInUser = AccountService.loggedInUser()
                angular.forEach(vm.comments, function (comment) {
                    if (loggedInUser.ID === comment.User.ID) {
                        comment.isOwner = true;
                    }
                });
            }, function (message) {
                vm.ErrorMessage = message;
            });
        }

        function edit(comment) {
            $state.go('forum.comments-edit', { id: $state.params.id, commentID: comment.ID });
        }

        function remove(comment) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your comment will be permanently removed.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, continue!"
            },
            function (isConfirm) {
                if (isConfirm) {
                    removeHelper(comment);
                }
            });
        }

        function removeHelper(comment) {
            CommentService.remove(comment.ID).then(function () {
                SweetAlert.swal({
                    title: "Removal Successful!",
                    text: "Your comment has been removed.",
                    type: "success",
                });
            }, function (error) {
                SweetAlert.swal({
                    title: "Removal Failed!",
                    text: "Your comment could not be removed at this time.",
                    type: "info",
                });
            });

        }

        function castVote(comment, upvote) {
            CommentService.castVote(upvote, comment.ID).then(function (vote) {
                comment.CommentVote = vote;
            }, function (message) {
                vm.VoteErrorMessage = message;
            });

        }

        function removeVote(comment) {
            CommentService.removeVote(comment.CommentVote).then(function (comment) {
                
            }, function (message) {
                vm.VoteErrorMessage = message;
            });

        }

        function voteFilter(vote)
        {
            return vote.IsUpvote;
        }
    }
})();
