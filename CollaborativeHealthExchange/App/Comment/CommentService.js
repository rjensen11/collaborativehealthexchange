﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .factory('CommentService', ['$http', 'ServerBase', '$q', '_', '$sce', CommentService]);


    function CommentService($http, ServerBase, $q, _, $sce) {
        var commentService = {
            getComment: getComment,
            getCommentsForForum: getCommentsForForum,
            add: add,
            edit: edit,
            remove: remove,
            castVote: castVote,
            removeVote: removeVote
        };
        commentService.url = ServerBase + 'Comments/';

        // ======== Private Properties ========
        var comments = [];

        return commentService;

        // ======== Function Definition ========
        function getComment(id) {
            return $http.get(commentService.url + 'Details/', { params: { id: id } }).then(function (response) {
                var comment = response.data;
                return updateCommentList(comment);
            });
        }

        function getCommentsForForum(id) {
            return $http.get(commentService.url + 'List/', { params: { id: id } }).then(function (response) {
                response = response.data;
                if (response.Status === "success") {
                    comments = response.Comments.map(function(comment) {
                        return transformCommentInformation(comment);
                    });
                    return comments;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('Fetching comments for this forum has failed.');
            });
        }

        function add(data) {
            return $http.post(commentService.url + "Add/", data).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    return updateCommentList(response.Comment);
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function(reply) {
                console.log(reply);
                return $q.reject('The comment failed to be added, please fill out all the valid fields and resubmit.');
            });
        }

        function edit(data) {
            return $http.post(commentService.url + "Edit/", data).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    return updateCommentList(response.Comment);
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (reply) {
                console.log(reply);
                return $q.reject('The forum failed to be edited, please fill out all the valid fields and resubmit.');
            });
        }

        function remove(id) {
            return $http.post(commentService.url + 'Remove/', { id: id }).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    var index = _.findIndex(comments, function (element) {
                        return element.ID == id;
                    });
                    if (index != -1) {
                        comments.splice(index, 1);
                    }
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                return $q.reject('The comment could not be deleted');
            });
        }

        function castVote(upvote, commentID) {
            var vote = {
                IsUpvote: upvote,
                CommentID: commentID
            };
            return $http.post(commentService.url + 'CastVote/', vote).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    var comment = findCachedComment(commentID);
                    vote.ID = response.ID;
                    comment.CommentVote = vote;
                    comment.CommentVotes.push(vote);
                    return vote;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('The comment could not be deleted');
            });
        }

        function removeVote(vote) {
            return $http.post(commentService.url + 'RemoveVote/', { id: vote.ID }).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    var comment = findCachedComment(vote.CommentID);
                    var index = _.findIndex(comment.CommentVotes, function (element) {
                        return element.ID == vote.ID;
                    });
                    if (index !== -1) {
                        comment.CommentVotes.splice(index, 1);
                    }
                    comment.CommentVote = null;
                    return comment;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('The comment could not be deleted');
            });
        }


        // =============== Private helper functions ====================

        function transformCommentInformation(comment) {
            comment.CreationDate = new Date(parseInt(comment.CreationDate.substr(6)));
            comment.EditDate = comment.EditDate ? new Date(parseInt(comment.EditDate.substr(6))) : comment.EditDate;
            comment.User.DateJoined = new Date(parseInt(comment.User.DateJoined.substr(6)));
            comment.BodyHtml = $sce.trustAsHtml(comment.Body);
            return comment;
        }

        function updateCommentList(comment) {
            var index = _.findIndex(comments, function (element) {
                return element.ID == comment.ID;
            });
            if (index != -1) {
                comments[index] = transformCommentInformation(comment);
            }
            else {
                comments.push(transformCommentInformation(comment));
            }
            return comment;
        }


        function findCachedComment(id) {
            var index = _.findIndex(comments, function (element) {
                return element.ID == id;
            });

            return (index !== -1) ? comments[index] : null;
        }
    }

})();