﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('LoginModalCtr', ['AccountService', '$scope', '$cookies', 'SweetAlert', 'toastr', 'OverrideAlert', 'AutoPopup', 'RegisterModal', LoginModalCtr]);

    function LoginModalCtr(AccountService, $scope, $cookies, SweetAlert, toastr, OverrideAlert, AutoPopup, RegisterModal) {
        var vm = this;
        vm.email = '';
        vm.password = '';
        vm.rememberMe = false;
        vm.login = login;
        vm.close = close;
        vm.register = register;

        activate();

        // ============ Implementation ============
        function activate() {
            vm.email = $cookies.get('rememberMe') || '';
            vm.password = '';
            vm.rememberMe = !!$cookies.get('rememberMe');
            vm.ErrorMessage = '';
            vm.InfoMessage = AutoPopup ? 'Please login to see this content.' : '';
        }

        function login() {
            AccountService.login({ email: vm.email, password: vm.password, rememberMe: vm.rememberMe }).then(function (user) {
                loginAlert();
                $scope.$close(user);
            }, function (error) {
                vm.ErrorMessage = 'The username/password combination was not found';
            });
        }

        function close() {
            if (!OverrideAlert) {
                SweetAlert.swal({
                    title: "Navigate Home?",
                    text: "Pending data may be lost",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, continue!"
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $scope.$dismiss('user.action');
                    }
                });
            }
            else {
                $scope.$dismiss('user.action');
            }
            
            
        }

        function register() {
            RegisterModal();
        }

        function loginAlert() {
            toastr["success"]("You have been successfully logged in.", "Logged in!", options);

        }

        var options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

    }
})();
