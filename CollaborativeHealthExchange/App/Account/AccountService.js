﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .factory('AccountService', ['$http', 'ServerBase', '$q', '_', '$rootScope', AccountService]);


    function AccountService($http, ServerBase, $q, _, $rootScope) {
        var accountService = {
            loggedInUser: function() {
                return loggedInUser;
            },
            login: login,
            logout: logout,
            register: register,
            resetPassword: function(){},
            forgotPassword: function(){}
        };
        accountService.url = ServerBase + 'Account/';

        // ======== Private Properties ========
        var loggedInUser;

        return accountService;

        // ======== Function Definition ========

        function login(data) {
            return $http.post(accountService.url + 'Login/', data).then(function (reply) {
                var response = reply.data;
                if (response.Status === 'success') {
                    loggedInUser = response.User;
                    $rootScope.$broadcast('user:loggedIn', loggedInUser);
                    return loggedInUser;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('The server encountered an error.');
            });

        }

        function logout() {
            return $http.post(accountService.url + 'Logout/').then(function (reply) {
                var response = reply.data;
                if (response.Status === 'success') {
                    loggedInUser = null;
                    $rootScope.$broadcast('user:loggedOut');
                    return response.Message;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('The server encountered an error.');
            });
        }

        function register(data) {
            return $http.post(accountService.url + 'Register/', data).then(function (reply) {
                var response = reply.data;
                if (response.Status === 'success') {
                    return;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('The server encountered an error.');
            });

        }
    }

})();