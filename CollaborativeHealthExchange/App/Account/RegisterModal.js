﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .factory('RegisterModal', ['$modal', '$state', RegisterModal]);


    function RegisterModal ($modal, $state) {

        return function () {
            var instance = $modal.open({
                templateUrl: 'Account/RegisterTemplate',
                controller: 'RegisterModalCtr as vm',
            });

            return instance.result
        };

    }

})();