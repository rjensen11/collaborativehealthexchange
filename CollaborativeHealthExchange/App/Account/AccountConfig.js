﻿(function () {
    'use strict';
    //angular.module('ChX').config(['$httpProvider', function ($httpProvider) {

    //    $httpProvider.interceptors.push(['$timeout', '$q', '$injector', function ($timeout, $q, $injector) {
    //        var loginModal, $http, $state;

    //        // this trick must be done so that we don't receive
    //        // `Uncaught Error: [$injector:cdep] Circular dependency found`
    //        $timeout(function () {
    //            LoginModal = $injector.get('LoginModal');
    //            $http = $injector.get('$http');
    //            $state = $injector.get('$state');
    //        });

    //        return {
    //            responseError: function (rejection) {
    //                if (rejection.status !== 401) {
    //                    return rejection;
    //                }

    //                var deferred = $q.defer();

    //                LoginModal()
    //                .then(function () {
    //                    deferred.resolve($http(rejection.config));
    //                }, function () {
    //                    $state.go('home');
    //                    deferred.reject(rejection);
    //                });

    //                return deferred.promise;
    //            }
    //        };
    //    }]);

    //}]);

    angular.module('ChX').run(['AccountService', '$rootScope', '$state', 'LoginModal', function (AccountService, $rootScope, $state, LoginModal) {

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
            var requireLogin = toState.data ? toState.data.requireLogin : false;

            
            if (requireLogin && !AccountService.loggedInUser()) {
                event.preventDefault();

                LoginModal(true)
                .then(function () {
                    return $state.go(toState.name, toParams, {reload:true});
                }, function () {
                    return $state.go('home');
                });

                if ($state.current.name === "") {
                    $state.go('home');
                }
            }
        });

    }]);

})();