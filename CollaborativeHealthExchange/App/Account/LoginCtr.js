﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('LoginCtr', ['AccountService', 'LoginModal', 'LogoutHelper', 'SweetAlert', 'toastr', '$state', LoginCtr]);

    function LoginCtr(AccountService, LoginModal, LogoutHelper, SweetAlert, toastr, $state) {
        var vm = this;
        vm.toggleLogin = toggleLogin;

        activate();
        
        // ============ Implementation ============
        function activate() {
            
        }

        function toggleLogin() {
            if (AccountService.loggedInUser()) {
                logout();
            }
            else {
                login();
            }
        }

        function login() {
            LoginModal().then(function () {
                $state.go($state.current, {}, { reload: true });
            });
        }

        function logout() {
            LogoutHelper.logout().then(function () {
                $state.go($state.current, {}, { reload: true });
            }, function (message) {

            });
        }

    }
})();
