﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .factory('LogoutHelper', ['AccountService', 'SweetAlert', 'toastr', '$q', LogoutHelper]);


    function LogoutHelper(AccountService, SweetAlert, toastr, $q) {
        var logoutHelper = {
            logout: logout,
        };

        // ======== Private Properties ========

        return logoutHelper;

        // ======== Function Definition ========

        function logout() {
            return AccountService.logout().then(function () {
                logoutAlert();
            }, function (message) {
                SweetAlert.swal({
                    title: "Logout Failed",
                    text: "The system failed to log out out. Please try again later."
                });
                $q.reject(message);
            });
        }

        function logoutAlert() {
            toastr["success"]("You have been successfully logged out.", "Logged out!", options);

        }

        var options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
    }

})();