﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .factory('LoginModal', ['$modal', '$state', LoginModal]);


    function LoginModal ($modal, $state) {

        return function (autoPopup) {
            autoPopup = !!autoPopup;
            var OverrideAlert = !($state.current.data ? $state.current.data.requireLogin : false);
            var instance = $modal.open({
                templateUrl: 'Account/LoginTemplate',
                controller: 'LoginModalCtr as vm',
                resolve: {
                    OverrideAlert: function () { return OverrideAlert; },
                    AutoPopup: function () { return autoPopup; },
                },
                backdrop: OverrideAlert ? true : 'static',
                keyboard: OverrideAlert
            });

            return instance.result;
        };

    }

})();