﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('RegisterModalCtr', ['AccountService', '$scope', 'toastr', 'Upload', RegisterModalCtr]);

    function RegisterModalCtr(AccountService, $scope, toastr, Upload) {
        var vm = this;
        

        vm.register = register;
        vm.close = close;
        vm.clearFile = clearFile;
        $scope.uploadFiles = uploadFiles;


        activate();

        // ============ Implementation ============
        function activate() {
            vm.email = '';
            vm.password = '';
            vm.confirmPassword = '';
            vm.firstname = '';
            vm.lastname = '';
            vm.ErrorMessage = '';
            vm.wait = false;
        }

        function register() {
            var data = {
                email: vm.email,
                password: vm.password,
                confirmPassword: vm.confirmPassword,
                firstname: vm.firstname,
                lastname: vm.lastname,
                profilePicture: vm.file
            };
            AccountService.register(data).then(function () {
                registeredAlert('Registration successful');
                $scope.$close();
            }, function (error) {
                vm.ErrorMessage = error;
            });
        }

        function close() {
            $scope.$dismiss('user.action');
        }

        function registeredAlert() {
            toastr["success"]("You have been registered successfully.", "Registered!", options);
        }

        var options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        function uploadFiles(file, errFiles) {
            vm.wait = true;
            Upload.dataUrl(file, true).then(function (fileData) {
                vm.file = fileData;
                vm.filename = file.name;
                vm.wait = false;
            });
            vm.errFile = errFiles && errFiles[0];
            //if (file) {
            //    file.upload = Upload.upload({
            //        url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
            //        data: { file: file }
            //    });

            //    file.upload.then(function (response) {
            //        $timeout(function () {
            //            file.result = response.data;
            //        });
            //    }, function (response) {
            //        if (response.status > 0)
            //            $scope.errorMsg = response.status + ': ' + response.data;
            //    }, function (evt) {
            //        file.progress = Math.min(100, parseInt(100.0 *
            //                                 evt.loaded / evt.total));
            //    });
            //}
        }

        function clearFile() {
            vm.file = null;
            vm.filename = '';
        }

    }
})();
