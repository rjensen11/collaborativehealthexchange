﻿(function () {
    'use strict';

    angular.module('ChX', [
        // Angular modules 
        'ngAnimate',
        'ui.router',
        'ngSanitize',
        'summernote',
        'oitozero.ngSweetAlert',
        'underscore',
        'ui.bootstrap',
        'ngCookies',
        'toastr',
        'ngFileUpload'
        // Custom modules 

        // 3rd Party Modules
        
    ]);

    angular.module('ChX').config(['$stateProvider', 'ServerBase', '$urlRouterProvider', function ($stateProvider, ServerBase, $urlRouterProvider) {
        $stateProvider
        .state('home', {
            url: '/Home',
            //template : '<p> This is the home page</p>'
            templateUrl: ServerBase + 'Home/HomeTemplate'
        })
        .state('user', {
            url: '/User/:id',
            template : '<p> This is the user page for user</p>'
        })
        .state('account', {
            url: '/Account',
            //template: '<p> This is the account page</p>'
            templateUrl: ServerBase + 'Home/AccountTemplate'
        });

        $urlRouterProvider.otherwise('/Home');

    }]);
    
})();

(function () {
    'use strict';

    angular.module('underscore', []);

    angular.module('underscore').factory('_', function () {
        return window._; //Underscore must already be loaded on the page
    });

})();

angular.element(document).ready(function () {
    angular.bootstrap(document, ['ChX', 'underscore']);
});