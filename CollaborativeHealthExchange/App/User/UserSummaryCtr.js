﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('UserSummaryCtr', ['UserService', 'AccountService', 'LogoutHelper', 'LoginModal', 'RegisterModal', '$state', '$scope', UserSummaryCtr]);

    function UserSummaryCtr(UserService, AccountService, LogoutHelper, LoginModal, RegisterModal, $state, $scope) {
        var vm = this;
        vm.login = login;
        vm.logout = logout;
        vm.register = register;
        
        activate();

        // ============ Implementation ============
        function activate() {
            var user = AccountService.loggedInUser();
            vm.isLoggedIn = user && user.ID;

            if (vm.isLoggedIn) {
                vm = angular.extend(vm, user);
            }
        }

        function login() {
            LoginModal().then(function () {
                $state.go($state.current, {}, { reload: true });
                activate();
            });
        }

        function logout() {
            LogoutHelper.logout().then(function () {
                $state.go($state.current, {}, { reload: true });
                activate();
            }, function (message) {

            });
        }

        function register() {
            RegisterModal().then(function () {
                login();
            });
        }

        $scope.$on('user:loggedIn', function () {
            vm.isLoggedIn = true;
            activate();
        });

        $scope.$on('user:loggedOut', function () {
            vm.isLoggedIn = false;
        });
    }
})();
