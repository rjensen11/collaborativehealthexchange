﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .factory('UserService', ['$http', 'ServerBase', '$q', '_', UserService]);


    function UserService($http, ServerBase, $q, _) {
        var userService = {
            quickSearch: quickSearch,
        };
        userService.url = ServerBase + 'Users/';

        // ======== Private Properties ========
        var users = [];

        return userService;

        // ======== Function Definition ========

        function quickSearch(searchTerm) {
            return $http.get(userService.url + 'QuickSearch/', { params: {search: searchTerm } }).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    users = response.Users.map(function (user) {
                        return transformUserInformation(user);
                    });
                    users.sort(defaultUserSort);
                    return users;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('Fetching users has failed.');
            });
        }


        // ======== Helper Functions ========
        function transformUserInformation(user) {
            user.FormattedName = user.FirstName + ' ' + user.LastName + ' (' + user.Email + ')';
            return user;
        }

        function updateUserList(user) {
            var index = _.findIndex(users, function (element) {
                return element.ID == user.ID;
            });
            if (index != -1) {
                users[index] = user;
            }
            else {
                users.push(user);
            }
            users.sort(defaultUserSort);
        }

        function defaultUserSort(first, second) {
            var s1 = first.LastName - second.LastName;
            return s1 != 0 ? s1 : first.FirstName - second.FirstName;
        }



    }

})();