﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('ForumDetailsCtr', ['ForumService', 'AccountService', '$state', 'SweetAlert', '_', '$q', ForumDetailsCtr]);

    function ForumDetailsCtr(ForumService, AccountService, $state, SweetAlert, _, $q) {
        var vm = this;
        vm.backToList = backToList;
        vm.edit = edit;
        vm.remove = remove;
        vm.ErrorMessage = '';
        vm.isOwner = false;
        vm.castVote = castVote;
        vm.removeVote = removeVote;
        vm.voteFilter = voteFilter;

        activate();

        // ============ Implementation ============
        function activate() { 
            ForumService.getForum($state.params.id).then(function(data) {
                vm = angular.extend(vm, data);
                
                vm.Views++;

                if (AccountService.loggedInUser().ID === vm.User.ID) {
                    vm.isOwner = true;
                }

                return vm.ID;
            }, function(message) {
                vm.ErrorMessage = message;
                $q.reject('Something went wrong');
            }).then(function(id) {
                return ForumService.updateViewsCount(id);
            });
        }

        function backToList() {
            $state.go('forum.list');
        }

        function edit() {
            $state.go('forum.edit', {id : vm.ID});
        }

        function remove() {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your forum will be permanently removed.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, continue!"
            },
            function (isConfirm) {
                if (isConfirm) {
                    removeHelper();
                }
            });
        }

        function removeHelper() {
            ForumService.remove(vm.ID).then(function (forum) {
                vm = angular.extend(vm, forum);
                SweetAlert.swal({
                    title: "Removal Successful!",
                    text: "Your forum has been removed. Comments will remain visible unless deleted by their user.",
                    type: "success",
                });
            }, function (error) {
                SweetAlert.swal({
                    title: "Removal Failed!",
                    text: "Your forum could not be removed at this time.",
                    type: "info",
                });
            });

        }

        function castVote(upvote) {
            ForumService.castVote(upvote, vm.ID).then(function (vote) {
                vm.ForumPostVote = vote;
            }, function (message) {
                vm.VoteErrorMessage = message;
            });

        }

        function removeVote() {
            ForumService.removeVote(vm.ForumPostVote).then(function (forum) {
                vm = angular.extend(vm, forum);
            }, function (message) {
                vm.VoteErrorMessage = message;
            });

        }

        function voteFilter(vote)
        {
            return vote.IsUpvote;
        }

    }
})();
