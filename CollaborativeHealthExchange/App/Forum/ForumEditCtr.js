﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('ForumEditCtr', ['ForumService', '$state', 'SweetAlert', ForumEditCtr]);

    function ForumEditCtr(ForumService, $state, SweetAlert) {
        var vm = this;
        vm.save = save;
        vm.cancel = cancel;

        vm.ErrorMessage = '';

        activate();

        // ============ Implementation ============
        function activate() {
            ForumService.getForum($state.params.id).then(function(data) {
                vm = angular.extend(vm, data);
                //vm.CreationDate = new Date(parseInt(vm.CreationDate.substr(6)));
                //vm.EditedDate = vm.EditedDate ? new Date(parseInt(vm.EditedDate.substr(6))) : vm.EditedDate;
                //vm.User.DateJoined = new Date(parseInt(vm.User.DateJoined.substr(6)));
            }, function(message) {
                vm.ErrorMessage = message;
            });
        }

        function save() {
            ForumService.edit(vm).then(function () {
                $state.go('forum.details', { id: vm.ID });
            }, function (failedMessage) {
                vm.ErrorMessage = failedMessage;
            });

        }

        function cancel() {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your progress will be lost",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, continue!"
            },
            function (isConfirm) {
                if (isConfirm) {
                    $state.go('forum.details', {id : vm.ID});
                }
            });
        }

        
        
    }
})();
