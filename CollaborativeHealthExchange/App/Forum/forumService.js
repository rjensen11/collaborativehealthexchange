﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .factory('ForumService', ['$http', 'ServerBase', '$q', '_', '$sce', ForumService]);


    function ForumService($http, ServerBase, $q, _, $sce) {
        // ======== Public API ========
        var forumService = {
            getForum: getForum,
            getList: getList,
            updateViewsCount: updateViewsCount,
            getTotalCount: 0,
            add: add,
            edit: edit,
            remove: remove,
            castVote: castVote,
            removeVote: removeVote
        };
        forumService.url = ServerBase + 'Forums/';

        // ======== Private Properties ========
        var forums = [];

        return forumService;

        // ======== Function Definition ========

        function getForum(id) {
            return $http.get(forumService.url + 'Details/', { params: { id: id } }).then(function (data) {
                data = data.data;
                return updateForumList(data);
            });
        }

        function updateViewsCount(id) {
            return $http.post(forumService.url + 'UpdateViewsCount/', { id: id }).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    var index = _.findIndex(forums, function (element) { return element.ID == id; });
                    if (index != -1) {
                        forums[index].Views = response.Views;
                    }
                    return response.Views;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (reply) {
                console.log(reply);
                return $q.reject('The forum failed to be updated.');
            });
        }

        function getList(parameters) {
            var params = {};
            params.page = parameters.page || 1;
            params.sort = parameters.sort || 'popular';
            params.t = parameters.t || 'week';
            if (parameters.search) {
                params.search = parameters.search;
            }
            
            return $http.get(forumService.url+'Search/', { params: params }).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    forums = response.Forums.map(function (forum) {
                        return transformForumInformation(forum);
                    });

                    forumService.getTotalCount = response.Total;
                    return { forums: forums, total: response.Total };
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('Fetching forums has failed.');
            });


            //var user = { ID: 1, Name: "Ryan Jensen" }
            //var user2 = { ID: 2, Name: "Brad Jensen" }

            //forums = [
            //    {ID: 1, BodySummary: "This is the first forum", Title: "Forum 1 Title", LastModified: "Yesterday", Score: 20, Comments: 30, User: user },
            //    {ID: 2, BodySummary: "This is the second forum", Title: "Forum 2 Title", LastModified: "Yesterday", Score: 22, Comments: 32, User: user2 }
            //];
            //forumService.getTotalCount = 2;
            //return $q.when({ forums: forums, total: 2 });
        }

        function add(data) {
            return $http.post(forumService.url + "Add/", data).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    data.ID = response.ID;
                    return data;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function(reply) {
                console.log(reply);
                return $q.reject('The forum failed to be added, please fill out all the valid fields and resubmit.');
            });
        }

        function edit(data) {
            return $http.post(forumService.url + "Edit/", data).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    //return updateForumList(data);
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (reply) {
                console.log(reply);
                return $q.reject('The forum failed to be edited, please fill out all the valid fields and resubmit.');
            });
        }

        function remove(id) {
            return $http.post(forumService.url + 'Remove/', { id: id }).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    updateForumList(response.Forum);
                    return response.Forum;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                return $q.reject('The forum could not be deleted');
            });
        }

        function castVote(upvote, forumID) {
            var vote = { 
                IsUpvote: upvote,
                ForumPostID: forumID
            };
            return $http.post(forumService.url + 'CastVote/', vote).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    var forum = findCachedForum(forumID);
                    vote.ID = response.ID;
                    forum.ForumPostVote = vote;
                    forum.ForumPostVotes.push(vote);
                    return vote;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('The forum could not be deleted');
            });
        }

        function removeVote(vote) {
            return $http.post(forumService.url + 'RemoveVote/', { id: vote.ID }).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    var forum = findCachedForum(vote.ForumPostID);
                    if (!forum) {
                        console.log('Find cached forum failed');
                        console.log(vote);
                    }

                    // Find in the list of all votes
                    var index = _.findIndex(forum.ForumPostVotes, function (element) {
                        return element.ID == vote.ID;
                    });

                    if (index !== -1) {
                        forum.ForumPostVotes.splice(index, 1);
                    }
                    // The logged in user's voting choice
                    forum.ForumPostVote = null;
                    return forum;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('The forum could not be deleted');
            });
        }

        // ======== Helper Functions ========
        function transformForumInformation(forum) {
            forum.CreationDate =  (forum.CreationDate && forum.CreationDate.indexOf('Date') != -1) ? new Date(parseInt(forum.CreationDate.substr(6))) : forum.CreationDate;
            forum.EditDate = (forum.EditDate && forum.EditDate.indexOf('Date') != -1) ? new Date(parseInt(forum.EditDate.substr(6))) : forum.EditDate;
            forum.User.DateJoined = (forum.User.DateJoined && forum.User.DateJoined.indexOf('Date') != -1) ? new Date(parseInt(forum.User.DateJoined.substr(6))) : null;
            forum.BodyHtml = $sce.trustAsHtml(forum.Body);
            return forum;
        }

        function updateForumList(forum) {
            var index = _.findIndex(forums, function (element) {
                return element.ID == forum.ID;
            });
            if (index != -1) {
                forums[index] = transformForumInformation(forum);
            }
            else {
                forums.push(transformForumInformation(forum));
            }
            return forum;
        }

        function findCachedForum(id) {
            var index = _.findIndex(forums, function (element) {
                return element.ID == id;
            });

            return (index !== -1) ? forums[index] : null;
        }
    }

})();