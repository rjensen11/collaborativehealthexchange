﻿(function () {
    'use strict';

    angular.module('ChX').config(['$stateProvider', 'ServerBase', function ($stateProvider, ServerBase) {

        $stateProvider.state('forum', {
            url:'/Forums',
            abstract: true,
            data: {
                requireLogin: true
            },
            views: {
                '': {
                    template: '<div ui-view="main"></div>'
                }
            }
        })
        .state('forum.list', {
            url: '?page&sort&t&search',
            views: {
                'main@forum': {
                    templateUrl: ServerBase + 'Forums/ListTemplate',
                    controller: 'ForumListCtr',
                    controllerAs: 'forums'
                }
            },
            reloadOnSearch: false
        })
        .state('forum.add', {
            url: '/Add',
            views: {
                'main@forum': {
                    templateUrl: ServerBase + 'Forums/AddTemplate',
                    controller: 'ForumAddCtr',
                    controllerAs: 'forum'
                }
            },
        })
        .state('forum.edit', {
            url: '/:id/Edit',
            views: {
                'main@forum': {
                    templateUrl: ServerBase + 'Forums/EditTemplate',
                    controller: 'ForumEditCtr',
                    controllerAs: 'forum'
                }
            }
        })
        .state('forum.details', {
            url: '/:id',
            views: {
                'main@forum': {
                    templateUrl: ServerBase + 'Forums/DetailsTemplate',
                    controller: 'ForumDetailsCtr',
                    controllerAs: 'forum'
                },
                'comments@forum.details': {
                    templateUrl: ServerBase + 'Comments/DetailsTemplate',
                    controller: 'CommentDetailsCtr',
                    controllerAs: 'comments'
                },
                'comments-add@forum.details': {
                    templateUrl: ServerBase + 'Comments/AddTemplate',
                    controller: 'CommentAddCtr',
                    controllerAs: 'comment'
                }
            },
        })
        .state('forum.comments-edit', {
            url: '/:id/Comment/:commentID/Edit',
            views: {
                'main@forum': {
                    templateUrl: ServerBase + 'Comments/EditTemplate',
                    controller: 'CommentEditCtr',
                    controllerAs: 'comment'
                }
            },
        })
    }]);
    
})();