﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('ForumAddCtr', ['ForumService', '$state', 'SweetAlert', ForumAddCtr]);

    function ForumAddCtr(ForumService, $state, SweetAlert) {
        var vm = this;
        vm.save = save;
        vm.cancel = cancel;

        vm.Title = '';
        vm.Body = '';
        vm.ErrorMessage = '';

        activate();

        // ============ Implementation ============
        function activate() {  }

        function save() {
            ForumService.add(vm).then(function (forum) {
                vm = angular.extend(vm, forum);
                $state.go('forum.details', { id: vm.ID });
            }, function (failedMessage) {
                vm.ErrorMessage = failedMessage;
            });

        }

        function cancel() {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your progress will be lost",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, continue!"
            },
            function (isConfirm) {
                if (isConfirm) {
                    $state.go('forum.list');
                }
            });
        }

        
        
    }
})();
