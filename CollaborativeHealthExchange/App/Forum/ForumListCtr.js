﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('ForumListCtr', ['ForumService', '$state', '$scope', 'LoginModal', ForumListCtr]);

    function ForumListCtr(ForumService, $state, $scope, LoginModal) {
        var vm = this;
        vm.search = search;
        vm.changePage = changePage;

        vm.title = 'Forum Topics';
        vm.searchTerm = $state.params.search;
        vm.page = $state.params.page;
        vm.t = $state.params.t;
        vm.sort = $state.params.sort;
        vm.searching = false;
        vm.forums = [];
        vm.pages = 1;


        activate();

        // ============ Implementation ============
        function activate() { search(); }

        // Search using the search params from the page
        function search() {
            vm.searching = true;
            // Set the parameters
            var params = {
                search: vm.searchTerm,
                page: vm.page,
                t: vm.t,
                sort: vm.sort
            };
            // change the url (if necessary)
            $state.go('.', params);
            ForumService.getList(params).then(function (listData) {
                vm.forums = listData.forums;
                vm.pages = Math.ceil(listData.total / 30);
            });
        }

        function changePage(page) {
            if (page > 0 && page <= vm.pages) {
                vm.page = page;
                search();
            }
        }
    }
})();
