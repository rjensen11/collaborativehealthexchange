﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('MessageReplyCtr', ['MessageService', 'AccountService', '$state', 'SweetAlert', '$scope', MessageReplyCtr]);

    function MessageReplyCtr(MessageService, AccountService, $state, SweetAlert, $scope) {
        var vm = this;
        vm.save = save;
        
        
        activate();

        // ============ Implementation ============
        function activate() {
            vm.Body = '';
            vm.ErrorMessage = '';
        }

        function save() {
            var user = AccountService.loggedInUser();
            var message = MessageService.getMessageCache();
            // If we are the original message sender then the original receiver is also receiving this reply
            // Otherwise this is a reply to the original message sender
            var receivingUserID = message.SendingUserID === user.ID ? message.ReceivingUserID : message.SendingUserID;
            MessageService.reply({ Body: vm.Body, MessageHeaderID: $state.params.id, ReceivingUserID: receivingUserID }).then(function () {
                activate();
            }, function (failedMessage) {
                vm.ErrorMessage = failedMessage;
            });
        }

    }
})();
