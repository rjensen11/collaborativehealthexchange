﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('MessageDetailsCtr', ['MessageService', '$state', '$sce', 'SweetAlert', MessageDetailsCtr]);

    function MessageDetailsCtr(MessageService, $state, $sce, SweetAlert) {
        var vm = this;
        vm.backToInbox = backToInbox;
        vm.remove = remove;
        vm.markUnread = markUnread;

        activate();

        // ============ Implementation ============
        function activate() {
            vm.ErrorMessage = '';

            MessageService.getMessage($state.params.id).then(function(message) {
                vm = angular.extend(vm, message);
            }, function(message) {
                vm.ErrorMessage = message;
            });
        }

        function backToInbox() {
            $state.go('message.list');
        }

        function markUnread() {
            MessageService.markUnread(vm.ID).then(function () {
                backToInbox();
            }, function (message) {
                SweetAlert.swal({
                    title: "Marking as unread Failed!",
                    text: "Your message could not be updated at this time.",
                    type: "info",
                });
            });
        }

        function remove(message) {
            var id = message.ID;
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your message will be moved to your trash.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, continue!"
            },
            function (isConfirm) {
                if (isConfirm) {
                    removeHelper(id);
                }
            });
        }

        function removeHelper(id) {
            MessageService.remove(id).then(function () {
                SweetAlert.swal({
                    title: "Removal Successful!",
                    text: "Your message has been removed.",
                    type: "success",
                });
            }, function (error) {
                SweetAlert.swal({
                    title: "Removal Failed!",
                    text: "Your message could not be removed at this time.",
                    type: "info",
                });
            });

        }

    }
})();
