﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .factory('MessageService', ['$http', 'ServerBase', '$q', '_', '$sce', '$rootScope', MessageService]);


    function MessageService($http, ServerBase, $q, _, $sce, $rootScope) {
        var messageService = {
            getMessageCache: function () { return messageCache; },
            getMessage: getMessage,
            getList: getList,
            getTotalCount: 0,
            add: add,
            remove: remove,
            markRead: function (id) { return mark(id, true); },
            markUnread: function (id) { return mark(id, false); },
            reply: reply,
            unread: unread, 

        };
        messageService.url = ServerBase + 'Messages/';

        // ======== Private Properties ========
        var messageCache = null;
        var messages = [];
        var unreadMessages = [];
        var activeParameters = {};


        return messageService;

        // ======== Function Definition ========

        function getMessage(id) {
            return $http.get(messageService.url + 'Detail/', { params: { id: id } }).then(function (data) {
                data = data.data;
                messageCache = data.Message;
                messageCache.Messages = messageCache.Messages.map(function (message) {
                    return transformMessageInformation(message);
                });

                updateReadStatus({ ID: id, ReceivingUserRead: true });

                

                return messageCache;
            });
        }

        function getList(parameters) {
            parameters = parameters || {};
            var params = {};
            params.page = parameters.page || 1;
            params.type = parameters.type || 'inbox';
            if (parameters.search) {
                params.search = parameters.search;
            }
            activeParameters = params;
            return $http.get(messageService.url + 'Search/', { params: params }).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {

                    messages = response.Messages.map(function(message) {
                        return transformMessageInformation(message);
                    });
                    messageService.getTotalCount = response.Total;

                    updateUnreadMessages(messages);

                    var returnData = { messages: response.Messages, total: response.Total };
                    return { messages: response.Messages, total: response.Total };
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('Fetching messages has failed.');
            });
        }

        function unread() {
            return $http.get(messageService.url + 'Unread/').then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {

                    unreadMessages = response.Messages.map(function (message) {
                        return transformMessageInformation(message);
                    });
                    updateMessages(unreadMessages);

                    return unreadMessages;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                console.log(error);
                return $q.reject('Fetching messages has failed.');
            });
        }

        function add(message) {
            return $http.post(messageService.url + "Add/", message).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    return response.MessageID;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (reply) {
                console.log(reply);
                return $q.reject('The message failed to be added, please fill out all the valid fields and resubmit.');
            });
        }

        function reply(message) {
            return $http.post(messageService.url + "Reply/", message).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    messageCache.Messages.push(transformMessageInformation(response.Message))
                    return response.Message;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (reply) {
                console.log(reply);
                return $q.reject('The message failed to be added, please fill out all the valid fields and resubmit.');
            });
        }

        function remove(id) {
            return $http.post(messageService.url + 'Remove/', { id: id }).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    var index = _.findIndex(messageCache.Messages, function (element) {
                        return element.ID == id;
                    });
                    if (index != -1) {
                        messageCache.Messages.splice(index, 1);
                    }


                    if (messageCache.Messages.length === 0) {
                        var index = _.findIndex(messages, function (element) {
                            return element.ID == messageCache.ID;
                        });
                        if (index != -1) {
                            messages.splice(index, 1);
                        }
                    }
                    return response.Message;
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                return $q.reject('The message could not be deleted');
            });
        }

        function mark(id, read) {
            read = !!read;
            return $http.post(messageService.url + 'Mark/', { id: id, read: read }).then(function (reply) {
                var response = reply.data;
                if (response.Status === "success") {
                    updateReadStatus({ ID: id, ReceivingUserRead: read });
                }
                else {
                    return $q.reject(response.Message);
                }
            }, function (error) {
                return $q.reject('The message could not be marked');
            });

        }

        function transformMessageInformation(message) {
            message.CreationDate = (message.CreationDate && message.CreationDate.indexOf('Date') != -1) ?
                                        new Date(parseInt(message.CreationDate.substr(6))) :
                                        message.CreationDate;

            if (message.Body) {
                message.BodyHtml = $sce.trustAsHtml(message.Body);
            }
            return message;
        }

        function updateReadStatus(message) {
            var index = _.findIndex(messages, function (element) {
                return element.ID == message.ID;
            });
            if (index != -1) {
                messages[index].ReceivingUserRead = message.ReceivingUserRead;
                updateUnreadMessages([]);
            }
        }

        function updateMessages(unreadMessages) {
            angular.forEach(unreadMessages, function (message) {
                var index = _.findIndex(messages, function (element) {
                    return element.ID == message.ID;
                });
                if (index != -1) {
                    messages[index] = message;
                }
                else {
                    messages.push(message);
                }
            });

            messages.sort(function (m1, m2) {
                return m1.CreationDate >= m2.CreationDate ? -1 : 1;
            });

            return messages;
        }

        function updateUnreadMessages(messages) {
            angular.forEach(messages, function (message) {
                var index = _.findIndex(unreadMessages, function (element) {
                    return element.ID == message.ID;
                });
                if (index != -1) {
                    unreadMessages[index] = message;
                }
                else {
                    unreadMessages.push(message);
                }
            });
            unreadMessages = unreadMessages.filter(function (message) {
                return !message.ReceivingUserRead;
            });
            unreadMessages.sort(function (m1, m2) {
                return m1.CreationDate >= m2.CreationDate ? -1 : 1;
            });
            $rootScope.$broadcast('unreadMessages:updated', unreadMessages);

            return unreadMessages;
        }

    }
})();