﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('MessageAddCtr', ['MessageService', 'UserService', '$state', 'SweetAlert', '$scope', MessageAddCtr]);

    function MessageAddCtr(MessageService, UserService, $state, SweetAlert, $scope) {
        var vm = this;
        vm.save = save;
        vm.cancel = cancel;
        vm.searchUsers = searchUsers;
        vm.noResults = true;
        vm.noResultsMessage = "No results";
        vm.loadingUsers = false;
        
        vm.To = null;
        vm.Subject = '';
        vm.Body = '';
        vm.ErrorMessage = '';
        activate();

        // ============ Implementation ============
        function activate() {  }

        function searchUsers(searchString) {
            return UserService.quickSearch(searchString).then(function (users) {
                vm.noResultsMessage = "No results";
                return users;
            });
        }
        //, function () {
        //    vm.noResultsMessage = "Server Error";
        //    return [];
        //}
        function save() {
            if (vm.To && typeof vm.To === 'object') {
                MessageService.add({ Body: vm.Body, Subject: vm.Subject, ReceivingUserID: vm.To.ID }).then(function (messageId) {
                    vm.ID = messageId;
                    $state.go('message.details', { id: vm.ID });
                }, function (failedMessage) {
                    vm.ErrorMessage = failedMessage;
                });
            }
            else {
                vm.ErrorMessage = 'You must compose a message to a valid user.';
            }
            

        }

        function cancel() {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your progress will be lost",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, continue!"
            },
            function (isConfirm) {
                if (isConfirm) {
                    $state.go('message.list');
                }
            });
        }

    }
})();
