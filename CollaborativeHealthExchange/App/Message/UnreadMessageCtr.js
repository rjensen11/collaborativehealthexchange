﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('UnreadMessageCtr', ['MessageService', '$state', '$scope', '$timeout', UnreadMessageCtr]);

    function UnreadMessageCtr(MessageService, $state, $scope, $timeout) {
        var vm = this;
        vm.messages = [];
        var timer = null;

        deactivate();

        // ============ Implementation ============
        function activate() {
            MessageService.unread().then(function (unreadMessages) {
                vm.messages = unreadMessages.slice(0, 10);
                vm.isActive = true;
            }).finally(function() {
                timer = $timeout(activate, 5 * 60 * 1000);
            });

        }

        function deactivate() {
            vm.isActive = false;
            if (timer) {
                $timeout.cancel(timer);
                timer = null;
            }
            vm.messages = [];
        }

        $scope.$on('user:loggedIn', function () {
            activate();
        });

        $scope.$on('user:loggedOut', function () {
            deactivate();
        });

        $scope.$on('unreadMessages:updated', function (event, unreadMessages) {
            vm.messages = unreadMessages.slice(0, 10);
        });
    }
})();
