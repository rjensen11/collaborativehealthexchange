﻿(function () {
    'use strict';

    angular
        .module('ChX')
        .controller('MessageListCtr', ['MessageService', '$state', '$scope', 'LoginModal', MessageListCtr]);

    function MessageListCtr(MessageService, $state, $scope, LoginModal) {
        var vm = this;
        vm.search = search;
        vm.changePage = changePage;
        vm.selectMessage = selectMessage;
        vm.isUnread = isUnread;

        vm.title = 'Message Topics';
        vm.searchTerm = $state.params.search;
        vm.page = $state.params.page;
        vm.type = $state.params.type;

        vm.searching = false;
        vm.messages = [];
        vm.pages = 1;


        activate();

        // ============ Implementation ============
        function activate() { search(); }

        // Search using the search params from the page
        function search() {
            vm.searching = true;
            // Set the parameters
            var params = {
                search: vm.searchTerm,
                page: vm.page,
                type: vm.type,
            };
            // change the url (if necessary)
            $state.go('.', params);
            MessageService.getList(params).then(function (listData) {
                vm.messages = listData.messages;
                angular.forEach(vm.messages, function (message) {
                    message.selected = false;
                });
                vm.pages = Math.ceil(listData.total / 30);
            });
        }

        function selectMessage(messageID) {
            $state.go('message.details', { id: messageID });
        }

        function isUnread(message) {
            return !message.ReceivingUserRead;
        }

        function changePage(page) {
            if (page > 0 && page <= vm.pages) {
                vm.page = page;
                search();
            }
        }
    }
})();
