﻿(function () {
    'use strict';

    angular.module('ChX').config(['$stateProvider', 'ServerBase', function ($stateProvider, ServerBase) {

        $stateProvider.state('message', {
            url:'/Messages',
            abstract: true,
            data: {
                requireLogin: true
            },
            views: {
                '': {
                    template: '<div ui-view="main"></div>'
                }
            }
        })
        .state('message.list', {
            url: '?type&page&search',
            views: {
                'main@message': {
                    templateUrl: ServerBase + 'Messages/ListTemplate',
                    controller: 'MessageListCtr',
                    controllerAs: 'messages'
                }
            },
            reloadOnSearch: false
        })
        .state('message.add', {
            url: '/Add',
            views: {
                'main@message': {
                    templateUrl: ServerBase + 'Messages/AddTemplate',
                    controller: 'MessageAddCtr',
                    controllerAs: 'message'
                }
            },
        })
        .state('message.details', {
            url: '/:id',
            views: {
                'main@message': {
                    templateUrl: ServerBase + 'Messages/DetailsTemplate',
                    controller: 'MessageDetailsCtr',
                    controllerAs: 'messages'
                },
                'message-reply@message.details': {
                    templateUrl: ServerBase + 'Messages/ReplyTemplate',
                    controller: 'MessageReplyCtr',
                    controllerAs: 'message'
                }
            },
        })
    }]);
    
})();