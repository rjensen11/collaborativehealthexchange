﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ChX
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Root",
                url: "",
                defaults: new { controller = "Application", action = "Layout", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "RootIndex",
            //    url: "Index.html",
            //    defaults: new { controller = "Application", action = "Index", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "{controller}", action = "{action}", id = UrlParameter.Optional }
            );


            //routes.MapRoute(
            //    name: "Login",
            //    url: "Login",
            //    defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "ResetPassword",
            //    url: "Password/Reset/{email}/{accessToken}",
            //    defaults: new { controller = "Account", action = "ResetPassword", email = UrlParameter.Optional, accessToken = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "Index",
            //    url: "{controller}",
            //    defaults: new { controller = "{controller}", action = "Index", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "List",
            //    url: "{controller}/List",
            //    defaults: new { controller = "{controller}", action = "List" }
            //);

            //routes.MapRoute(
            //    name: "Add",
            //    url: "{controller}/Add",
            //    defaults: new { controller = "{controller}", action = "Add", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "Detail",
            //    url: "{controller}/Detail/{id}",
            //    defaults: new { controller = "{controller}", action = "Index", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "{controller}", action = "{action}", id = UrlParameter.Optional }
            //);
        }
    }
}
