﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ChX.Helpers
{
    public static class LabelForHelper
    {
        public static MvcHtmlString LabelForRequired<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes, string helperText = "")
        {
            return LabelForRequired(html, expression, new RouteValueDictionary(htmlAttributes), helperText);
        }

        public static MvcHtmlString LabelForRequired<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes, string helperText = "")
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            string labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            if (String.IsNullOrEmpty(labelText))
            {
                return MvcHtmlString.Empty;
            }

            TagBuilder tag = new TagBuilder("label");
            tag.MergeAttributes(htmlAttributes);
            tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));

            var sb = new StringBuilder();
            sb.Append(labelText);

            if (metadata.IsRequired)
            {
                TagBuilder span = new TagBuilder("span");
                span.AddCssClass("text-danger");
                span.Attributes.Add("style", "display:inline");
                span.InnerHtml = "&nbsp;*";
                sb.Append(span.ToString(TagRenderMode.Normal));
            }

            if (helperText != "")
            {
                TagBuilder span = new TagBuilder("span");
                span.AddCssClass("naFormLabelHelper");
                span.InnerHtml = helperText;
                sb.Append(span.ToString(TagRenderMode.Normal));
            }

            // assign <span> to <label> inner html
            tag.InnerHtml = sb.ToString();

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
    }
}