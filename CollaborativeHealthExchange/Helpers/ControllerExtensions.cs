﻿using ChX.Models.Entitys;
using nextAIMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChX.Helpers
{
    public static class ControllerBaseExtension
    {

        public static User GetLoggedInUser(this ControllerBase controller)
        {
            return new UserManager().LoggedInUser;
        }
    }
}