﻿	<html>
    <body style="color:grey; font-size:15px;">
    <font face="Helvetica, Arial, sans-serif">

    <div style="background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;">

	<p>
		Dear {0},
	</p>

	<p>
		A request has been made to reset you nextAIMS account password.  If this request was made in error or you did not initiate this request, you may safely ignore it.
	</p>

	<p>
		To reset your password please click the following link and follow the steps to choose a new personal password which will be your password for the whole nextAIMS system:
	</p>
	<p>
		<a href="{2}">{2}</a>
	</p>
	<p>
		If you have any questions please feel free to contact XXXXXXXXX who would be happy to answer whatever questions you have.
	</p>
	<p>
		Sincerily,<br />
		The nextAIMS Team
	</p>
    </div>
    </body>
	</html>