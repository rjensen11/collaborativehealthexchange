﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using ChX.Models.Entitys;

namespace nextAIMS.Models
{
    public class UserManager
    {

        private string _timingAttackDefaultHash;

        public User LoggedInUser
        {
            get
            {
                return GetCurrentSession()["User"] as User;
            }
        }

        public UserManager()
        {
            _timingAttackDefaultHash = new PasswordHasher().HashPassword("nextAIMS");
        }

        public SignInStatus AttemptUserLogin(User user, string PlainPassword)
        {
            if (user != null && user.PassHash != null)
            {
                if (CheckPasswords(user.PassHash, PlainPassword) == PasswordVerificationResult.Success)
                {
                    LoginUser(user);
                    return SignInStatus.Success;
                }

            }

            var timingAttackDudPasswordHashResult = new PasswordHasher().VerifyHashedPassword(_timingAttackDefaultHash, "nextAIMS");

            return SignInStatus.Failure;
        }

        public PasswordVerificationResult CheckPasswords(string PasswordHash, string PlainPassword)
        {
            return new PasswordHasher().VerifyHashedPassword(PasswordHash, PlainPassword);
        }

        private void LoginUser(User user)
        {
            user.LastLogin = DateTime.Now;
            GetCurrentSession()["User"] = user;
        }

        public void LogoutUser()
        {
            // Clear out the current session
            // TODO: Update database with the fact that the user logged out 
            GetCurrentSession()["User"] = null;
        }

        public bool IsCurrentUserLoggedIn()
        {
            User user = LoggedInUser;
            return (user != null && !user.ShouldResetPasswordOnLogin);
        }

        private SignInStatus AttemptLoginWithAccessToken(User user, string accessToken)
        {
            if (user.TokenValidUntil != null && 
                ((DateTime) user.TokenValidUntil).CompareTo(DateTime.Now) > 0 && 
                user.AccessToken.Equals(accessToken))
            {
                return SignInStatus.Success;
            }
            return SignInStatus.Failure;
        }

        // Reasons for failure:
        // - User not found
        // - Access Token doesn't match database
        public SignInStatus AttemptResetPassword(User user, string password, string accessToken)
        {

            if (user == null)
            {
                return SignInStatus.Failure;
            }

            // Attempt to validate the user based on access token
            if (AttemptLoginWithAccessToken(user, accessToken).Equals(SignInStatus.Success))
            {
                // Cancel the onetime use token
                user.AccessToken = null;
                // Unset ShouldResetPasswordOnLogin
                user.ShouldResetPasswordOnLogin = false;
                // Create a new password
                user.PassHash = new PasswordHasher().HashPassword(password);
                LoginUser(user);

                return SignInStatus.Success;
            }
            return SignInStatus.Failure;
        }

        public bool ValidatePasswords(string password, string confirmPassword)
        {
            if (password == null || confirmPassword == null)
            {
                return false;
            }
            // TODO: Add more validation rules
            return password.Equals(confirmPassword);
        }

        public string GenerateRandomTokenForUser(User user, int expirationTime = 3)
        {
            string accessToken = GenerateRandomToken();
            user.AccessToken = accessToken;
            user.TokenValidUntil = DateTime.Now.AddHours(expirationTime);

            return accessToken;
        }

        public string GetResetPasswordLink(User user)
        {
            return new UrlHelper(HttpContext.Current.Request.RequestContext).Action("ResetPassword", "Account", new { username = user.Email, accessToken = user.AccessToken }, "http");
        }

        public string HashPassword(string PlaintextPassword)
        {
            return new PasswordHasher().HashPassword(PlaintextPassword);
        }

        private string GenerateRandomToken()
        {
            string AccessToken = Membership.GeneratePassword(64, 0);
            Random rand = new Random();
            AccessToken = Regex.Replace(AccessToken, @"[^a-zA-Z0-9]", m => rand.Next(0, 10).ToString());
            return AccessToken;
        }

        private HttpSessionState GetCurrentSession()
        {
            return HttpContext.Current.Session;
        }

        
    }
}