﻿using ChX.Models.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChX.Models.Stores
{
    public class ForumPostVoteStore : UniqueKeyEntityStore<ForumPostVote>
    {
        public ForumPostVoteStore() : base()
        {

        }

        public ForumPostVoteStore(ChXDBContext context) : base(context)
        {

        }
    }
}