﻿using ChX.Models.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChX.Models.Stores
{
    public class CommentVoteStore : UniqueKeyEntityStore<CommentVote>
    {
        public CommentVoteStore() : base()
        {

        }

        public CommentVoteStore(ChXDBContext context) : base(context)
        {

        }
    }
}