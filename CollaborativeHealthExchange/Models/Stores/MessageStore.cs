﻿using ChX.Models.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChX.Models.Stores
{
    public class MessageStore : UniqueKeyEntityStore<Message>
    {
        public MessageStore() : base()
        {

        }

        public MessageStore(ChXDBContext context) : base(context)
        {

        }
    }
}