﻿using ChX.Models.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChX.Models.Stores
{
    public class ForumPostStore : UniqueKeyEntityStore<ForumPost>
    {
        public ForumPostStore() : base()
        {

        }

        public ForumPostStore(ChXDBContext context) : base(context)
        {

        }

        public IEnumerable<ForumPost> GetPostList(ForumSearchRequest request)
        {
            
            if (request.Sort == "new")
            {
                return (from f in Repository
                        where (
                            string.IsNullOrEmpty(request.SearchTerm) || 
                            f.Body.Contains(request.SearchTerm) || 
                            f.Title.Contains(request.SearchTerm)
                        )
                        && (
                            !request.QueryTime.HasValue || 
                            f.CreationDate > request.QueryTime.Value
                        )
                        orderby f.CreationDate descending
                        select f);
            }
            else
            {
                return (from f in Repository
                        where (
                            string.IsNullOrEmpty(request.SearchTerm) ||
                            f.Body.Contains(request.SearchTerm) ||
                            f.Title.Contains(request.SearchTerm)
                        )
                        && (
                            !request.QueryTime.HasValue || 
                            f.CreationDate > request.QueryTime.Value
                        )
                        select f)
                .OrderByDescending(f => f.ForumPostVotes.Sum(v => (v.IsUpvote ? 1 : -1)));

            }

        }
    }

    public class ForumSearchRequest
    {
        public string SearchTerm { get; set; }

        public string Sort { get; set; }

        public string T { get; set; }

        public int Page { get; set; }

        public DateTime? QueryTime {
            get
            {
                switch (T)
                {
                    case "day":
                        return DateTime.Now.AddDays(-1);
                    case "week":
                        return DateTime.Now.AddDays(-7);
                    case "month":
                        return DateTime.Now.AddMonths(-1);
                    case "year":
                        return DateTime.Now.AddYears(-1);
                    default:
                        return null;
                }
            }
        }

    }
}