﻿using ChX.Models.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChX.Models.Stores
{
    public class UserStore : UniqueKeyEntityStore<User>
    {
        public UserStore() : base()
        {

        }

        public UserStore(ChXDBContext context) : base(context)
        {

        }

        public User GetUserByEmail(string Email)
        {
            User user = (from u in _dbContext.Users
                         where u.Active && 
                            u.Email == Email
                         select u).FirstOrDefault();
            return user;
        }

        public IEnumerable<User> QuickSearch(string term1, string term2)
        {
            var results = (
                from u in _dbContext.Users
                where u.Active &&
                    u.Email.Contains(term1) ||
                    u.LastName.Contains(term1) ||
                    u.FirstName.Contains(term1)
                select u
            );

            if (term2 != null)
            {
                results = (
                    from u in results
                    where 
                        u.Email.Contains(term2) ||
                        u.LastName.Contains(term2) ||
                        u.FirstName.Contains(term2)
                    select u
                );
            }

            return results;
        }
    }
}