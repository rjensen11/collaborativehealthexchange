﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ChX.Models.Stores
{
    public interface IEntityStore<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAll<TKey>(Expression<Func<TEntity, TKey>> orderBy, ListSortDirection ascending);
        void Add(TEntity entity);
        void Add(IEnumerable<TEntity> entitys);
        void Save();
    }
}
