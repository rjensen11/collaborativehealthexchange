﻿using ChX.Models.Entitys;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ChX.Models.Stores
{
    public abstract class UniqueKeyEntityStore<TEntity> : EntityStore<TEntity>, IDisposable where TEntity : class, IUniqueKeyEntity
    {

        public UniqueKeyEntityStore() : base()
        {

        }

        public UniqueKeyEntityStore(ChXDBContext context) : base(context)
        {

        }

        public TEntity GetByID(int id)
        {
            return (from s in _dbSet
                    where s.ID == id
                    select s).FirstOrDefault();
        }
    }
}