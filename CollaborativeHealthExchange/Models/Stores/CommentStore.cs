﻿using ChX.Models.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChX.Models.Stores
{
    public class CommentStore : UniqueKeyEntityStore<Comment>
    {
        public CommentStore() : base()
        {

        }

        public CommentStore(ChXDBContext context) : base(context)
        {

        }
    }
}