﻿using ChX.Models.Entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ChX.Models.Stores
{
    public abstract class EntityStore<TEntity> : IEntityStore<TEntity>, IDisposable where TEntity : class
    {
        public ChXDBContext Context { get { return _dbContext; } }
        protected ChXDBContext _dbContext;
        public DbSet<TEntity> Repository { get { return _dbSet; } }
        protected DbSet<TEntity> _dbSet;

        public EntityStore() : this(new ChXDBContext())
        {

        }

        public EntityStore(ChXDBContext context)
        {
            _dbContext = context;
            _dbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            var data = _dbSet;
            return data as IEnumerable<TEntity>;
        }

        public virtual IEnumerable<TEntity> GetAll<TKey>(Expression<Func<TEntity, TKey>> orderBy, ListSortDirection ascending = ListSortDirection.Ascending)
        {
            var data = (ascending == ListSortDirection.Ascending ? _dbSet.OrderBy(orderBy) : _dbSet.OrderByDescending(orderBy));
            return data as IEnumerable<TEntity>;
        }

        public virtual IEnumerable<TEntity> GetAllWhere<TKey>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TKey>> orderBy, ListSortDirection ascending = ListSortDirection.Ascending)
        {
            var data = (ascending == ListSortDirection.Ascending ? _dbSet.Where(predicate).OrderBy(orderBy) : _dbSet.Where(predicate).OrderByDescending(orderBy));
            return data as IEnumerable<TEntity>;
        }

        public virtual IEnumerable<TEntity> GetAllWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate) as IEnumerable<TEntity>;
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public void Add(IEnumerable<TEntity> entitys)
        {
            foreach (var entity in entitys)
            {
                _dbSet.Add(entity);
            }
        }

        public void Attach(TEntity entity)
        {
            _dbSet.Attach(entity);
        }

        public void Attach(IEnumerable<TEntity> entitys)
        {
            foreach (var entity in entitys)
            {
                _dbSet.Attach(entity);
            }
        }

        public virtual void Remove(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public void Save()
        {
            try
            {
                _dbContext.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }




    }
}