﻿using ChX.Models.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChX.Models.Stores
{
    public class RoleStore : UniqueKeyEntityStore<Role>
    {
        public RoleStore() : base()
        {

        }

        public RoleStore(ChXDBContext context) : base(context)
        {

        }
    }
}