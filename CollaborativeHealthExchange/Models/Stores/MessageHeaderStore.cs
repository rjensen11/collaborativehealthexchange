﻿using ChX.Models.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChX.Models.Stores
{
    public class MessageHeaderStore : UniqueKeyEntityStore<MessageHeader>
    {
        public MessageHeaderStore() : base()
        {

        }

        public MessageHeaderStore(ChXDBContext context) : base(context)
        {

        }

        public IEnumerable<MessageHeader> GetMessageList(MessageHeaderRequest request)
        {
            throw new NotImplementedException("You gotta wait for this functionality");
        }

        public IEnumerable<MessageHeader> UnreadMessages(User user)
        {
            var messages = (from m in Repository
                            where (m.ReceivingUserID == user.ID && !m.ReceivingUserRead) ||
                                  (m.SendingUserID == user.ID && !m.SendingUserRead) &&
                                   m.Messages.Any(message =>
                                        (message.ReceivingUserID == user.ID && !message.ReceivingUserDelete) ||
                                        (message.SendingUserID == user.ID && !message.SendingUserDelete))
                            orderby m.CreationDate descending
                            select m);
            return messages;

        }
    }

    public class MessageHeaderRequest
    {
        public string search { get; set; }

        public string type { get; set; }

        public int page { get; set; }
    }
}