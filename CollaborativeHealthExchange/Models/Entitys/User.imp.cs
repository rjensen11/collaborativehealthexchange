namespace ChX.Models.Entitys
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;

    public partial class User : IUniqueKeyEntity
    {
        public long CalculateReputation()
        {
            var fVotes = ForumPosts.SelectMany(f => f.ForumPostVotes);
            var cVotes = Comments.SelectMany(c => c.CommentVotes);

            var fUpvotes = fVotes.Where(v => v.IsUpvote).Count();
            var fDownvotes = fVotes.Count() - fUpvotes;
            var cUpvotes = cVotes.Where(v => v.IsUpvote).Count();
            var cDownvotes = cVotes.Count() - cUpvotes;
            var reputation = (3 * fUpvotes + cUpvotes) - (fDownvotes + cDownvotes / 3);
            return Math.Max(reputation, 0);
        }
    }
}
