namespace ChX.Models.Entitys
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ForumPostVote : IUniqueKeyEntity
    {
        public static int CalculateScore(IEnumerable<ForumPostVote> votes)
        {
            int forumScore = 0;
            foreach (var vote in votes)
            {
                forumScore = vote.IsUpvote ? forumScore + 1 : forumScore - 1;
            }
            return forumScore;
        }
    }
}
