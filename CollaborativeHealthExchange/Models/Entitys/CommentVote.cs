namespace ChX.Models.Entitys
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CommentVote")]
    public partial class CommentVote : IUniqueKeyEntity
    {

        public CommentVote()
        {
            CreationDate = DateTime.Now;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public bool IsUpvote { get; set; }

        public int UserID { get; set; }

        public int CommentID { get; set; }

        public DateTime CreationDate { get; set; }

        public virtual Comment Comment { get; set; }

        public virtual User User { get; set; }
    }
}
