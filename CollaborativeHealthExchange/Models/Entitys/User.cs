namespace ChX.Models.Entitys
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User : IUniqueKeyEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            Active = true;
            DateJoined = DateTime.Now;

            Comments = new HashSet<Comment>();
            CommentVotes = new HashSet<CommentVote>();
            ForumPosts = new HashSet<ForumPost>();
            ForumPostVotes = new HashSet<ForumPostVote>();
            SentMessages = new HashSet<Message>();
            ReceivedMessages = new HashSet<Message>();
            Roles = new HashSet<Role>();
            SendMessageHeaders = new HashSet<MessageHeader>();
            ReceivedMessageHeaders = new HashSet<MessageHeader>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(300)]
        public string Email { get; set; }

        [StringLength(75)]
        public string PassHash { get; set; }

        [Required]
        [StringLength(75)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(75)]
        public string LastName { get; set; }

        public bool Active { get; set; }

        public bool ShouldResetPasswordOnLogin { get; set; }

        [StringLength(75)]
        public string AccessToken { get; set; }

        public DateTime? TokenValidUntil { get; set; }

        public bool HasVerifyedEmail { get; set; }

        public DateTime? DateJoined { get; set; }

        public DateTime? LastLogin { get; set; }

        public bool Removed { get; set; }

        public DateTime? RemovedDate { get; set; }

        public bool IsSystemAdmin { get; set; }

        public string ProfilePicture { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CommentVote> CommentVotes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForumPost> ForumPosts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForumPostVote> ForumPostVotes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Message> SentMessages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Message> ReceivedMessages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MessageHeader> SendMessageHeaders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Role> Roles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MessageHeader> ReceivedMessageHeaders { get; set; }
    }
}
