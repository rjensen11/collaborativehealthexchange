namespace ChX.Models.Entitys
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ChXDBContext : DbContext
    {
        public ChXDBContext()
            : base("name=ChXDBContext")
        {
        }

        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<CommentVote> CommentVotes { get; set; }
        public virtual DbSet<ForumPost> ForumPosts { get; set; }
        public virtual DbSet<ForumPostVote> ForumPostVotes { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<MessageHeader> MessageHeaders { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>()
                .HasMany(e => e.CommentVotes)
                .WithRequired(e => e.Comment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ForumPost>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.ForumPost)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ForumPost>()
                .HasMany(e => e.ForumPostVotes)
                .WithRequired(e => e.ForumPost)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Roles)
                .Map(m => m.ToTable("UserRole").MapLeftKey("RoleID").MapRightKey("UserID"));

            modelBuilder.Entity<User>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            
            modelBuilder.Entity<User>()
                .HasMany(e => e.CommentVotes)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ForumPosts)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ForumPostVotes)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SentMessages)
                .WithRequired(e => e.SendingUser)
                .HasForeignKey(e => e.SendingUserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ReceivedMessages)
                .WithRequired(e => e.ReceivingUser)
                .HasForeignKey(e => e.ReceivingUserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SendMessageHeaders)
                .WithRequired(e => e.SendingUser)
                .HasForeignKey(e => e.SendingUserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ReceivedMessageHeaders)
                .WithRequired(e => e.ReceivingUser)
                .HasForeignKey(e => e.ReceivingUserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MessageHeader>()
                .HasMany(e => e.Messages)
                .WithRequired(e => e.MessageHeader)
                .HasForeignKey(e => e.MessageHeaderID)
                .WillCascadeOnDelete(false);
        }
    }
}
