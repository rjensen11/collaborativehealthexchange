﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChX.Models.Entitys
{
    public interface IUniqueKeyEntity
    {
        int ID { get; set; }
    }
}
