namespace ChX.Models.Entitys
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ForumPostVote")]
    public partial class ForumPostVote : IUniqueKeyEntity
    {
        public ForumPostVote()
        {
            CreationDate = DateTime.Now;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public bool IsUpvote { get; set; }

        public int UserID { get; set; }

        public int ForumPostID { get; set; }

        public DateTime CreationDate { get; set; }

        public virtual ForumPost ForumPost { get; set; }

        public virtual User User { get; set; }
    }
}
