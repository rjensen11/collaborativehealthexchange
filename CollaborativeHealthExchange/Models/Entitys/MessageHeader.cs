namespace ChX.Models.Entitys
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MessageHeader")]
    public partial class MessageHeader : IUniqueKeyEntity
    {
        public MessageHeader()
        {
            CreationDate = DateTime.Now;
            Messages = new HashSet<Message>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(200)]
        public string Subject { get; set; }

        public int SendingUserID { get; set; }

        public int ReceivingUserID { get; set; }

        public DateTime CreationDate { get; set; }

        public bool SendingUserRead { get; set; }

        public bool ReceivingUserRead { get; set; }

        public virtual User SendingUser { get; set; }

        public virtual User ReceivingUser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Message> Messages { get; set; }
    }
}
