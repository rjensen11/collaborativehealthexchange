namespace ChX.Models.Entitys
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ForumPost")]
    public partial class ForumPost : IUniqueKeyEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ForumPost()
        {
            Edited = false;
            CreationDate = DateTime.Now;

            Comments = new HashSet<Comment>();
            ForumPostVotes = new HashSet<ForumPostVote>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public string Body { get; set; }

        [Required]
        [StringLength(300)]
        public string Title { get; set; }

        public int UserID { get; set; }

        public int Views { get; set; }

        public bool Edited { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? EditDate { get; set; }

        public bool Removed { get; set; }

        public DateTime? RemovedDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForumPostVote> ForumPostVotes { get; set; }
    }
}
