namespace ChX.Models.Entitys
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Message")]
    public partial class Message : IUniqueKeyEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Message()
        {
            CreationDate = DateTime.Now;

        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public string Body { get; set; }

        public int SendingUserID { get; set; }

        public int ReceivingUserID { get; set; }

        public int MessageHeaderID { get; set; }

        public DateTime CreationDate { get; set; }

        public bool SendingUserDelete { get; set; }

        public bool ReceivingUserDelete { get; set; }

        public virtual User SendingUser { get; set; }

        public virtual User ReceivingUser { get; set; }

        public virtual MessageHeader MessageHeader { get; set; }
    }
}
