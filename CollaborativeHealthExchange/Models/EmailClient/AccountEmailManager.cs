﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using ChX.Models.Entitys;

namespace ChX.Models.EmailClient
{
    public class AccountEmailManager
    {
        public AccountEmailManager()
        {

        }
       
        public void SendAccountSetupEmail(User user, string singleUseLoginString)
        {
            //Read template file from the App_Data folder
            using (var sr = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/Templates/SendAccountSetupEmailTemplate.txt")))
            {
                string body = sr.ReadToEnd();
                string sender = ConfigurationManager.AppSettings["EmailFromAddress"];
                string emailSubject = "nextAIMS - Account Setup";

                string messageBody = string.Format(body, user.FirstName, user.Email, singleUseLoginString);

                var MailHelper = new MailHelper
                {
                    Sender = sender,
                    Recipient = user.Email,
                    RecipientCC = null,
                    Subject = emailSubject,
                    Body = messageBody
                };
                MailHelper.Send();
            }
        }

        public void SendForgotPasswordEmail(User user, string singleUseLoginString)
        {
            //Read template file from the App_Data folder
            using (var sr = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/Templates/SendForgotPasswordEmailTemplate.txt")))
            {
                string body = sr.ReadToEnd();
                string sender = ConfigurationManager.AppSettings["EmailFromAddress"];
                string emailSubject = "nextAIMS - Account Setup";

                string messageBody = string.Format(body, user.FirstName, user.Email, singleUseLoginString);

                var MailHelper = new MailHelper
                {
                    Sender = sender,
                    Recipient = user.Email,
                    RecipientCC = null,
                    Subject = emailSubject,
                    Body = messageBody
                };
                MailHelper.Send();
            }
        }
    }
}


