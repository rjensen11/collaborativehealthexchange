﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChX.Controllers
{
    public class FormsController : Controller
    {

        public ActionResult FormsElements()
        {
            return View();
        }

        public ActionResult FormsExtended()
        {
            return View();
        }

        public ActionResult TextEditor()
        {
            return View();
        }

        public ActionResult Wizard()
        {
            return View();
        }

    }
}
