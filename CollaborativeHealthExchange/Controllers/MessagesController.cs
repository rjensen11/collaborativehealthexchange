﻿using ChX.Models.Stores;
using ChX.ViewModels;
using nextAIMS.Models;
using nextAIMS.Models.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChX.Controllers
{
    public class MessagesController : Controller
    {

        [HttpGet]
        public ActionResult AddTemplate()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult DetailsTemplate()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult ReplyTemplate()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult ListTemplate()
        {
            return PartialView();
        }

        [HttpPost]
        [AuthorizeUser]
        public ActionResult Add(MessageAddViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Status = "failed", Message = "The model did not successfully validate, please correct the errors and resubmit" });
            }
            using (var messageHeaderStore = new MessageHeaderStore())
            {
                var messageHeader = model.MessageFactory();
                messageHeaderStore.Add(messageHeader);
                messageHeaderStore.Save();

                return Json(new { MessageID = messageHeader.ID, Status = "success", Message = "The message was successfully validated" });
            }
        }

        [HttpGet]
        [AuthorizeUser]
        public ActionResult Search(MessageHeaderRequest request)
        {
            using (var messageHeaderStore = new MessageHeaderStore())
            {
                //var data = forumStore.GetPostList(request);
                //var count = data.Count();
                //var vms = data.Skip((request.Page - 1) * 30).Take(30).Select(model => new ForumSummaryViewModel(model)).ToList();

                var user = new UserManager().LoggedInUser;
                var messages = messageHeaderStore.GetAllWhere(
                    m => m.Messages.Any(message => message.ReceivingUserID == user.ID && !message.ReceivingUserDelete))
                    .Select(m => new MessageSummaryViewModel(m)).OrderByDescending(m => m.CreationDate).ToList();

                return Json(new { Messages = messages, Total = messages.Count(), Status = "success" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AuthorizeUser]
        public ActionResult Unread()
        {
            using (var messageHeaderStore = new MessageHeaderStore())
            {
                var user = new UserManager().LoggedInUser;
                var messages = messageHeaderStore.UnreadMessages(user)
                    .Select(m => new MessageSummaryViewModel(m)).ToList();

                return Json(new { Messages = messages, Status = "success" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Detail(int id)
        {
            using (var messageStore = new MessageHeaderStore())
            {
                var user = new UserManager().LoggedInUser;
                var messageHeader = messageStore.GetByID(id);

                // ======= Check message conditions =======
                if (messageHeader == null)
                {
                    return Json(new { Status = "failed", Message = "The message was not found." }, JsonRequestBehavior.AllowGet);
                }
                if (messageHeader.SendingUserID != user.ID && messageHeader.ReceivingUserID != user.ID)
                {
                    return Json(new { Status = "failed", Message = "You are not authorized to view this message." }, JsonRequestBehavior.AllowGet);
                }

                // Get messages where you are the receiver and it isn't deleted or where you are the sender and it isn't deleted
                var messages = messageHeader.Messages.Where(m => (m.ReceivingUserID == user.ID && !m.ReceivingUserDelete) || (m.SendingUserID == user.ID && !m.SendingUserDelete));
                if (messages.Count() == 0)
                {
                    return Json(new { Status = "failed", Message = "The message has been deleted." }, JsonRequestBehavior.AllowGet);
                }

                // ======= Change message state =======
                if (messageHeader.ReceivingUserID == user.ID)
                {
                    messageHeader.ReceivingUserRead = true;
                }
                if (messageHeader.SendingUserID == user.ID)
                {
                    messageHeader.SendingUserRead = true;
                }
                messageStore.Save();

                // ======= Populate model =======
                var model = new MessageHeaderViewModel(messageHeader, messages.OrderBy(m => m.CreationDate));
                return Json(new { Status = "success", Message = model }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Reply(MessageReplyViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Status = "failed", Message = "The model did not successfully validate, please correct the errors and resubmit" });
            }
            using (var messageStore = new MessageStore())
            using (var messageHeaderStore = new MessageHeaderStore(messageStore.Context))
            using (var userStore = new UserStore(messageStore.Context))
            {
                var message = model.MessageFactory();
                var mh = messageHeaderStore.GetByID(model.MessageHeaderID.Value);
                if (message.ReceivingUserID == mh.ReceivingUserID)
                {
                    mh.ReceivingUserRead = false;
                }
                else
                {
                    mh.SendingUserRead = false;
                }
                messageStore.Add(message);
                messageStore.Save();

                message.ReceivingUser = userStore.GetByID(message.ReceivingUserID);
                message.SendingUser = userStore.GetByID(message.SendingUserID);

                return Json(new { Message = new MessageDetailViewModel(message), Status = "success" });
            }
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            
            using (var messageStore = new MessageStore())
            {
                var message = messageStore.GetByID(id);
                if (message == null)
                {
                    return Json(new { Status = "failed", Message = "The message was not found." });
                }

                var user = new UserManager().LoggedInUser;
                if (user == null ||  (user.ID != message.ReceivingUserID && user.ID != message.SendingUserID))
                {
                    return Json(new { Status = "failed", Message = "Only the owner of the message can remove it." });
                }

                if (user.ID == message.ReceivingUserID)
                {
                    message.ReceivingUserDelete = true;
                }
                if (user.ID == message.SendingUserID)
                {
                    message.SendingUserDelete = true;
                }

                messageStore.Save();

                return Json(new { Status = "success", Message = "The message was successfully removed" });
            }
        }

        [HttpPost]
        public ActionResult Mark(int id, bool read)
        {

            using (var messageStore = new MessageHeaderStore())
            {
                var message = messageStore.GetByID(id);
                if (message == null)
                {
                    return Json(new { Status = "failed", Message = "The message was not found." });
                }

                var user = new UserManager().LoggedInUser;
                if (user == null || (user.ID != message.ReceivingUserID && user.ID != message.SendingUserID))
                {
                    return Json(new { Status = "failed", Message = "Only the owner of the message can modify it." });
                }

                if (user.ID == message.ReceivingUserID)
                {
                    message.ReceivingUserRead = read;
                }
                if (user.ID == message.SendingUserID)
                {
                    message.SendingUserRead = read;
                }

                messageStore.Save();

                return Json(new { Status = "success", Message = "The message was successfully marked" });
            }
        }

        //[HttpGet]
        //public ActionResult Index(int? id)
        //{
        //    if (id == null)
        //    {
        //        return RedirectToAction("List");
        //    }
        //    using (var messageStore = new MessageStore())
        //    {
        //        return View(new MessageIndexViewModel(messageStore.GetByID(id.Value)));
        //    }
        //}

        //[HttpGet]
        //public ActionResult Add()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Add(MessageAddViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    using (var messageStore = new MessageStore())
        //    using (var userStore = new UserStore(messageStore.Context))
        //    {
        //        var message = model.AddMessage(userStore);
        //        messageStore.Add(message);
        //        messageStore.Save();

        //        return RedirectToAction("Add");
        //    }
        //}

        //[HttpGet]
        //public ActionResult List()
        //{
        //    using (var messageStore = new MessageStore())
        //    {
        //        var vm = new MessageListViewModel();
        //        vm.Messages = messageStore.GetAll(m => m.CreationDate, ListSortDirection.Descending).Select(messageModel => new MessageSummaryViewModel(messageModel));

        //        return View(vm);
        //    }
        //}


    }
}
