﻿using ChX.Models.EmailClient;
using ChX.Models.Entitys;
using ChX.Models.Stores;
using ChX.ViewModels;
using nextAIMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChX.Controllers
{
    public class UsersController : Controller
    {

        [HttpGet]
        public ActionResult QuickSearch(string search)
        {
            search = search.Trim();
            if (search == null || search.Length == 0)
            {
                return Json(new { Status = "failed", Message = "No search term" });
            }
            var terms = search.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var t1 = terms[0];
            var t2 = terms.Length > 1 ? terms[1] : null;


            using (var userStore = new UserStore())
            {
                var users = userStore.QuickSearch(t1, t2).Take(20).Select(userModel => new UserSummaryViewModel(userModel)).ToList();
                return Json(new { Users = users, Status = "success" }, JsonRequestBehavior.AllowGet);
            }
        }


        //[HttpGet]
        //public ActionResult Index(int? userID)
        //{
        //    User user = new UserManager().LoggedInUser;

        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Account");
        //    }

        //    if (userID == null || userID != user.ID)
        //    {
        //        return RedirectToAction("Index", new { userID = user.ID });
        //    }

        //    using (var userStore = new UserStore())
        //    {
        //        userStore.Attach(user);

        //        return View(new UserIndexViewModel(user));
        //    }
        //}

        //[HttpGet]
        //public ActionResult Add(bool? didSendEmail = null)
        //{
        //    ViewBag.DidSendEmail = didSendEmail;
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Add(UserAddViewModel model)
        //{
        //    // If the modelstate is valid, an access token should be genereated or the passwords match we should NOT short circuit this action
        //    if (!ModelState.IsValid)
        //    {
        //        ModelState.AddModelError("", "Please correct the validation errors and resubmit the form.");
        //        return View(model);
        //    }

        //    var userManager = new UserManager();

        //    if (!model.ShouldGenerateAccessToken && !userManager.ValidatePasswords(model.Password, model.ConfirmPassword))
        //    {
        //        ModelState.AddModelError("", "Please correct the validation errors and resubmit the form.");
        //        return View(model);
        //    }
        //    using (var userStore = new UserStore())
        //    using (var roleStore = new RoleStore(userStore.Context))
        //    {
        //        if (userStore.GetUserByEmail(model.Email) != null)
        //        {
        //            ModelState.AddModelError("", "We are sorry this username has been taken. You can reset your password by following the forgot password link from the login page.");
        //            model.Email = "";
        //            return View(model);
        //        }


        //        var user = model.AddUser(roleStore);

        //        userStore.Add(user);
        //        userStore.Save();

        //        bool emailSent = model.ShouldGenerateAccessToken;

        //        if (model.ShouldGenerateAccessToken)
        //        {
        //            userManager.GenerateRandomTokenForUser(user);
        //            userStore.Save();
        //            new AccountEmailManager().SendAccountSetupEmail(user, userManager.GetResetPasswordLink(user));
        //        }

        //        return RedirectToAction("Add", new { DidSendEmail = emailSent });
        //    }

        //}

        //[HttpGet]
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return RedirectToAction("Index");
        //    }

        //    using (var userStore = new UserStore())
        //    {
        //        return View(new UserEditViewModel(userStore.GetByID(id.Value)));
        //    }
        //}

        //[HttpPost]
        //public ActionResult Edit(UserEditViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    using (var userStore = new UserStore())
        //    using (var roleStore = new RoleStore(userStore.Context))
        //    {
        //        var forumPost = model.EditUser(userStore.GetByID(model.ID), roleStore);
        //        userStore.Save();

        //        return RedirectToAction("Index", new { id = model.ID });
        //    }
        //}

        //[HttpGet]
        //public ActionResult List()
        //{
        //    using (var userStore = new UserStore())
        //    {
        //        var vm = new UserListViewModel();
        //        vm.Users = userStore.GetAll(m => m.LastName, ListSortDirection.Descending).Select(userModel => new UserSummaryViewModel(userModel));
        //        return View(vm);
        //    }
        //}

        //[HttpGet]
        //public ActionResult ForumPosts(int? userID)
        //{
        //    User loggedInUser = new UserManager().LoggedInUser;
            
        //    if (userID == null)
        //    {
        //        if (loggedInUser == null)
        //        {
        //            return RedirectToAction("Login", "Account");
        //        }
        //        return RedirectToAction("Index", new { userID = loggedInUser.ID });
        //    }

        //    using (var userStore = new UserStore())
        //    {
        //        var user = userStore.GetByID(userID.Value);
        //        var vm = new ForumUserListViewModel(user);
        //        vm.ForumPosts = new ForumListViewModel(user.ForumPosts);

        //        return View(vm);
        //    }
        //}

        //[HttpGet]
        //public ActionResult Comments(int? userID)
        //{
        //    User loggedInUser = new UserManager().LoggedInUser;

        //    if (userID == null)
        //    {
        //        if (loggedInUser == null)
        //        {
        //            return RedirectToAction("Login", "Account");
        //        }
        //        return RedirectToAction("Index", new { userID = loggedInUser.ID });
        //    }

        //    using (var userStore = new UserStore())
        //    {
        //        var user = userStore.GetByID(userID.Value);
        //        var vm = new CommentUserListViewModel(user);
        //        vm.Comments = new CommentListViewModel(user.Comments);

        //        return View(vm);
        //    }
        //}

        //[HttpGet]
        //public ActionResult Messages(string type)
        //{
        //    var user = new UserManager().LoggedInUser;
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Account");
        //    }

        //    using (var userStore = new UserStore())
        //    {
        //        userStore.Attach(user);
        //        IEnumerable<Message> messages = null;
        //        switch (type)
        //        {
        //            case null:
        //            case "inbox":
        //            case "recieved":
        //                messages = user.ReceivedMessages.Where(m => !m.ReceivingUserDelete && m.ParentMessageID == null).OrderByDescending(m => m.CreationDate);
        //                break;
        //            case "sent":
        //                messages = user.SendMessages.Where(m => !m.SendingUserDelete && m.ParentMessageID == null).OrderByDescending(m => m.CreationDate);
        //                break;
        //            default:
        //                break;
        //        }
        //        var vm = new MessageUserListViewModel(user, messages);
        //        return View(vm);
        //    }
        //}

    }
}