﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChX.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult HomeTemplate()
        {
            return PartialView();
        }

        public ActionResult AccountTemplate()
        {
            return PartialView();
        }
    }
}