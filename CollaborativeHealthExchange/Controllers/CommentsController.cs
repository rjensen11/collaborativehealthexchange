﻿using ChX.Models.Stores;
using ChX.ViewModels;
using nextAIMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChX.Controllers
{
    public class CommentsController : Controller
    {

        [HttpGet]
        public ActionResult DetailsTemplate()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult AddTemplate()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult EditTemplate()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            using (var commentStore = new CommentStore())
            {
                var comment = commentStore.GetByID(id);
                if (comment == null)
                {
                    return Json(new { Status = "failed", Message = "The forum was not found." }, JsonRequestBehavior.AllowGet);
                }
                if (comment.Removed)
                {
                    return Json(new { Status = "failed", Message = "The comment has been removed." }, JsonRequestBehavior.AllowGet);
                }
                return Json(new CommentDetailViewModel(comment), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult List(int id)
        {
            using (var forumStore = new ForumPostStore())
            {
                var forum = forumStore.GetByID(id);
                if (forum == null)
                {
                    return Json(new { Status = "failed", Message = "The forum was not found." }, JsonRequestBehavior.AllowGet);
                }
                var comments = forum.Comments.Where(c => !c.Removed).OrderBy(c => c.CreationDate).Select(c => new CommentDetailViewModel(c)).ToList();
                return Json(new { Comments = comments, Status = "success" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Add(CommentAddViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Status = "failed", Message = "The model did not successfully validate, please correct the errors and resubmit" });
            }
            using (var commentStore = new CommentStore())
            using (var userStore = new UserStore(commentStore.Context))
            {
                var comment = model.CommentFactory();
                commentStore.Add(comment);
                commentStore.Save();
                comment.User = userStore.GetByID(comment.UserID);
                return Json(new { Comment = new CommentDetailViewModel(comment), Status = "success", Message = "The forum was successfully validated" });
            }
        }

        [HttpPost]
        public ActionResult Edit(CommentEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Status = "failed", Message = "The model did not successfully validate, please correct the errors and resubmit" });
            }
            
            using (var commentStore = new CommentStore())
            {
                var comment = commentStore.GetByID(model.ID);
                if (comment == null)
                {
                    return Json(new { Status = "failed", Message = "No comment with the given ID exists." });
                }
                if (comment.Removed)
                {
                    return Json(new { Status = "failed", Message = "The comment has been removed." });
                }

                var user = new UserManager().LoggedInUser;
                if (user == null || user.ID != comment.User.ID)
                {
                    return Json(new { Status = "failed", Message = "Only the owner of the comment can edit it." });
                }

                comment = model.CommentFactory(comment);
                commentStore.Save();

                return Json(new { Comment = new CommentDetailViewModel(comment), Status = "success", Message = "The forum was successfully validated" });
            }
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            using (var commentStore = new CommentStore())
            {
                var comment = commentStore.GetByID(id);
                if (comment == null)
                {
                    return Json(new { Status = "failed", Message = "The forum was not found." });
                }

                var user = new UserManager().LoggedInUser;
                if (user == null || user.ID != comment.User.ID)
                {
                    return Json(new { Status = "failed", Message = "Only the owner of the comment can remove it." });
                }

                comment.Removed = true;
                comment.RemovedDate = DateTime.Now;

                commentStore.Save();

                return Json(new { Status = "success", Message = "The comment has successfully been removed" });
            }
        }

        [HttpPost]
        public ActionResult CastVote(CommentVoteAddViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Status = "failed", Message = "The model did not successfully validate, please correct the errors and resubmit" });
            }
            using (var voteStore = new CommentVoteStore())
            using (var commentStore = new CommentStore(voteStore.Context))
            {
                var user = new UserManager().LoggedInUser;
                var comment = commentStore.GetByID(model.CommentID);
                if (comment != null && comment.UserID == user.ID)
                {
                    return Json(new { Status = "failed", Message = "You cannot vote on your own content" });
                }


                var votes = voteStore.GetAllWhere(v => v.UserID == user.ID && v.CommentID == model.CommentID);

                if (votes.Count() > 0)
                {
                    foreach (var v in votes)
                    {
                        voteStore.Remove(v);
                    }
                }
                var vote = model.CommentVoteFactory();
                voteStore.Add(vote);

                voteStore.Save();

                return Json(new { ID = vote.ID, Status = "success", Message = "The vote was successfully validated" });
            }
        }

        [HttpPost]
        public ActionResult RemoveVote(int id)
        {
            using (var voteStore = new CommentVoteStore())
            {
                var vote = voteStore.GetByID(id);
                if (vote == null)
                {
                    return Json(new { Status = "failed", Message = "No vote for id" });
                }

                var user = new UserManager().LoggedInUser;
                if (vote.UserID != user.ID)
                {
                    return Json(new { Status = "failed", Message = "User doesn't have correct permissions to remove this vote." });
                }


                voteStore.Remove(vote);
                voteStore.Save();

                return Json(new { Status = "success", Message = "The vote was successfully removed" });
            }
        }

        //// GET: Comments
        //[HttpGet]
        //public ActionResult Index(int? id)
        //{
        //    if (id == null)
        //    {
        //        return RedirectToAction("List");
        //    }
        //    using (var commentStore = new CommentStore())
        //    {
        //        return View(new CommentIndexViewModel(commentStore.GetByID(id.Value)));
        //    }
        //}

        //[HttpGet]
        //public ActionResult Add()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Add(CommentAddViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    using (var commentStore = new CommentStore())
        //    {
        //        var comment = model.AddComment();
        //        commentStore.Add(comment);
        //        commentStore.Save();

        //        return RedirectToAction("Add");
        //    }
        //}

        //[HttpGet]
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return RedirectToAction("List");
        //    }
        //    using (var commentStore = new CommentStore())
        //    {
        //        return View(new CommentEditViewModel(commentStore.GetByID(id.Value)));
        //    }
        //}

        //[HttpPost]
        //public ActionResult Edit(CommentEditViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    using (var commentStore = new CommentStore())
        //    {
        //        var comment = model.EditComment(commentStore.GetByID(model.ID));
        //        commentStore.Save();

        //        return RedirectToAction("Index", new { id = model.ID });
        //    }

        //}

        //[HttpGet]
        //public ActionResult List(int forumID)
        //{
        //    using (var commentStore = new CommentStore())
        //    {
        //        var vm = new CommentListViewModel();
        //        vm.Comments = commentStore.GetAllWhere(m => m.ForumPostID == forumID, m => m.CreationDate, ListSortDirection.Descending).Select(commentModel => new CommentSummaryViewModel(commentModel));
        //        return View(vm);
        //    }
        //}


        //[HttpGet]
        //public ActionResult Saved(string type)
        //{
        //    var user = new UserManager().LoggedInUser;
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Account");
        //    }

        //    using (var userStore = new UserStore())
        //    {
        //        var vm = new CommentUserListViewModel(user);
        //        switch (type)
        //        {
        //            case "liked":
        //                vm.Comments = new CommentListViewModel(user.CommentVotes.Where(v => v.IsUpvote).Select(v => v.Comment));
        //                break;
        //            case "disliked":
        //                vm.Comments = new CommentListViewModel(user.CommentVotes.Where(v => !v.IsUpvote).Select(v => v.Comment));
        //                break;
        //            default:
        //                vm.Comments = new CommentListViewModel(user.CommentVotes.Select(v => v.Comment));
        //                break;
        //        }


        //        return View(vm);
        //    }
        //}
    }
}