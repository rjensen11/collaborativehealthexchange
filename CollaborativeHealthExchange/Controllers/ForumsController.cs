﻿using ChX.Models.Entitys;
using ChX.Models.Stores;
using ChX.ViewModels;
using nextAIMS.Models;
using nextAIMS.Models.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChX.Controllers
{
    public class ForumsController : Controller
    {
        [HttpGet]
        public ActionResult ListTemplate()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult AddTemplate()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult DetailsTemplate()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult EditTemplate()
        {
            return PartialView();
        }

        [HttpPost]
        [AuthorizeUser]
        public ActionResult Add(ForumAddViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Status = "failed", Message = "The model did not successfully validate, please correct the errors and resubmit"});
            }
            using (var forumStore = new ForumPostStore())
            {
                var forum = model.ForumPostFactory();
                forumStore.Add(forum);
                forumStore.Save();

                return Json(new { ID = forum.ID, Status = "success", Message = "The forum was successfully validated" });
            }
        }

        [HttpPost]
        [AuthorizeUser]
        public ActionResult Edit(ForumEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Status = "failed", Message = "The model did not successfully validate, please correct the errors and resubmit" });
            }
            using (var forumStore = new ForumPostStore())
            {
                var forum = forumStore.GetByID(model.ID);
                if (forum == null)
                {
                    return Json(new { Status = "failed", Message = "No forum with the given ID exists." });
                }
                if (forum.Removed)
                {
                    return Json(new { Status = "failed", Message = "The forum has been removed." });
                }

                var user = new UserManager().LoggedInUser;
                if (user == null || user.ID != forum.User.ID)
                {
                    return Json(new { Status = "failed", Message = "Only the owner of the forum can edit it." });
                }
                model.ForumPostFactory(forum);
                forumStore.Save();

                return Json(new { Status = "success", Message = "The forum was successfully validated" });
            }
        }

        [HttpGet]
        [AuthorizeUser]
        public ActionResult Search(ForumSearchRequest request)
        {
            
            using (var forumStore = new ForumPostStore())
            {
                //var data = forumStore.GetPostList(request);
                //var count = data.Count();
                //var vms = data.Skip((request.Page - 1) * 30).Take(30).Select(model => new ForumSummaryViewModel(model)).ToList();

                var data = forumStore.GetAll(f => f.CreationDate, ListSortDirection.Descending).Where(f => !f.Removed);
                var count = data.Count();
                var vms = data.Select(model => new ForumSummaryViewModel(model)).ToList();

                return Json(new { Forums = vms, Total = count, Status = "success" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AuthorizeUser]
        public ActionResult Details(int id)
        {
            using (var forumStore = new ForumPostStore())
            {
                var forum = forumStore.GetByID(id);
                if (forum == null)
                {
                    return Json(new { Status = "failed", Message = "The forum was not found." }, JsonRequestBehavior.AllowGet);
                }

                var vm = new ForumIndexViewModel(forum);

                if (forum.Removed)
                {
                    vm = new ForumIndexViewModel
                    {
                        ID = vm.ID,
                        User = new UserSummaryViewModel
                        {
                            ID = 0,
                            FirstName = "",
                            LastName = "[Deleted]",
                            Email = "[Deleted]"
                        },
                        Body = "[Deleted]",
                        Title = "[Deleted]",
                    };
                }
                return Json(vm, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [AuthorizeUser]
        public ActionResult UpdateViewsCount(int id)
        {
            using (var forumStore = new ForumPostStore())
            {
                var forum = forumStore.GetByID(id);
                if (forum == null)
                {
                    return Json(new { Status = "failed", Message = "The forum was not found." });
                }
                forum.Views++;
                forumStore.Save();
                return Json(new { Views = forum.Views, Status = "success"});
            }
        }

        [HttpPost]
        [AuthorizeUser]
        public ActionResult Remove(int id)
        {
            using (var forumStore = new ForumPostStore())
            {
                var forum = forumStore.GetByID(id);
                if (forum == null)
                {
                    return Json(new { Status = "failed", Message = "The forum was not found." });
                }

                var user = new UserManager().LoggedInUser;
                if (user == null || user.ID != forum.User.ID)
                {
                    return Json(new { Status = "failed", Message = "Only the owner of the forum can remove it." });
                }

                forum.Removed = true;
                forum.RemovedDate = DateTime.Now;

                forumStore.Save();

                var vm = new ForumIndexViewModel
                {
                    ID = forum.ID,
                    User = new UserSummaryViewModel
                    {
                        ID = 0,
                        FirstName = "",
                        LastName = "[Deleted]",
                        Email = "[Deleted]"
                    },
                    Body = "[Deleted]",
                    Title = "[Deleted]",
                };


                return Json(new { Forum = vm, Status = "success" });
            }
        }

        [HttpPost]
        public ActionResult CastVote(ForumVoteAddViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Status = "failed", Message = "The model did not successfully validate, please correct the errors and resubmit" });
            }

            using (var voteStore = new ForumPostVoteStore())
            using (var forumStore = new ForumPostStore(voteStore.Context))
            {
                var user = new UserManager().LoggedInUser;
                var forum = forumStore.GetByID(model.ForumPostID);
                if (forum != null && forum.UserID == user.ID)
                {
                    return Json(new { Status = "failed", Message = "You cannot vote on your own content" });
                }


                var votes = voteStore.GetAllWhere(v => v.UserID == user.ID && v.ForumPostID == model.ForumPostID);

                if (votes.Count() > 0)
                {
                    foreach (var v in votes)
                    {
                        voteStore.Remove(v);
                    }
                }
                var vote = model.ForumPostVoteFactory();
                voteStore.Add(vote);

                voteStore.Save();

                return Json(new { ID = vote.ID, Status = "success", Message = "The vote was successfully validated" });
            }
        }

        [HttpPost]
        public ActionResult RemoveVote(int id)
        {
            using (var voteStore = new ForumPostVoteStore())
            {
                var vote = voteStore.GetByID(id);
                if (vote == null)
                {
                    return Json(new { Status = "failed", Message = "No vote for id" });
                }

                var user = new UserManager().LoggedInUser;
                if (vote.UserID != user.ID)
                {
                    return Json(new { Status = "failed", Message = "User doesn't have correct permissions to remove this vote." });
                }


                voteStore.Remove(vote);
                voteStore.Save();

                return Json(new { Status = "success", Message = "The vote was successfully removed" });
            }
        }

        //[HttpGet]
        //public ActionResult Index(int? id)
        //{
        //    if (id == null)
        //    {
        //        return RedirectToAction("List");
        //    }
        //    using (var forumStore = new ForumPostStore())
        //    {
        //        return View(new ForumIndexViewModel(forumStore.GetByID(id.Value)));
        //    }
        //}

        //[HttpGet]
        //public ActionResult Add()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Add(ForumAddViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    using (var forumStore = new ForumPostStore())
        //    {
        //        var forumPost = model.AddForumPost();
        //        forumStore.Add(forumPost);
        //        forumStore.Save();
        //        return RedirectToAction("Add");
        //    }
        //}

        //[HttpGet]
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return RedirectToAction("List");
        //    }
        //    using (var forumStore = new ForumPostStore())
        //    {
        //        return View(new ForumEditViewModel(forumStore.GetByID(id.Value)));
        //    }
        //}

        //[HttpPost]
        //public ActionResult Edit(ForumEditViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    using (var forumStore = new ForumPostStore())
        //    {
        //        var forumPost = model.EditForumPost(forumStore.GetByID(model.ID));
        //        forumStore.Save();

        //        return RedirectToAction("Index", new { id = model.ID });
        //    }

        //}


        //[HttpGet]
        //public ActionResult List()
        //{
        //    using (var forumStore = new ForumPostStore())
        //    {
        //        var vm = new ForumListViewModel();
        //        vm.ForumPosts = forumStore.GetAll(m => m.CreationDate, ListSortDirection.Descending).Select(forumModel => new ForumSummaryViewModel(forumModel));
        //        return View(vm);
        //    }
        //}


        //[HttpGet]
        //public ActionResult Saved(string type)
        //{
        //    var user = new UserManager().LoggedInUser;
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Account");
        //    }

        //    using (var userStore = new UserStore())
        //    {
        //        var vm = new ForumUserListViewModel(user);
        //        switch (type)
        //        {
        //            case "liked":
        //                vm.ForumPosts = new ForumListViewModel(user.ForumPostVotes.Where(v => v.IsUpvote).Select(v => v.ForumPost));
        //                break;
        //            case "disliked":
        //                vm.ForumPosts = new ForumListViewModel(user.ForumPostVotes.Where(v => !v.IsUpvote).Select(v => v.ForumPost));
        //                break;
        //            default:
        //                vm.ForumPosts = new ForumListViewModel(user.ForumPostVotes.Select(v => v.ForumPost));
        //                break;
        //        }


        //        return View(vm);
        //    }
        //}
    }
}
