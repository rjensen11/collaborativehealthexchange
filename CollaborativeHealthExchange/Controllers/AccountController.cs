﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using nextAIMS;
using nextAIMS.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using ChX.Models.EmailClient;
using ChX.Models.Stores;
using ChX.Models.Entitys;
using ChX.Models.AccountManagement;
using ChX.ViewModels;

namespace ChX.Controllers
{
    public class AccountController : Controller
    {


        public AccountController()
        {
        }

        [HttpGet]
        public ActionResult LoginTemplate()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult RegisterTemplate()
        {
            return PartialView();
        }


        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model)
        {
            // Manage rememberMe cookie state
            if (model.RememberMe)
            {
                // If the rememberMe box is checked add a cookie
                HttpCookie rememberMeCookie = new HttpCookie("rememberMe", (model.Email ?? ""));
                rememberMeCookie.Expires = DateTime.Now.AddDays(365);
                Response.AppendCookie(rememberMeCookie);
            }
            else if (Request.Cookies["rememberMe"] != null)
            {
                // Else clear out any existing cookies
                HttpCookie myCookie = new HttpCookie("rememberMe");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }

            // Check that required fields are entered
            if (!ModelState.IsValid)
            {
                return Json(new { Status = "failed", Message = "Please correct validation errors and resubmit" });
            }

            using (var userStore = new UserStore())
            {
                User user = userStore.GetUserByEmail(model.Email);
                // Attempt login
                var result = new UserManager().AttemptUserLogin(user, model.Password);
                userStore.Save();
                switch (result)
                {
                    case SignInStatus.Success:
                        //// If they signed in successfully first check with the status of the forced password reset
                        //if (user.ShouldResetPasswordOnLogin)
                        
                        return Json(new { Status = "success", User = new UserSummaryViewModel(user), Message = "You have successfully logged in!" });
                    case SignInStatus.Failure:
                    default:
                        return Json(new { Status = "failed", Message = "The email/password combination is not recognized." });
                }
            }
        }

        [HttpPost]
        public ActionResult Logout()
        {
            var um = new UserManager();
            if (um.IsCurrentUserLoggedIn())
            {
                um.LogoutUser();
                return Json(new { Status = "success", Message = "You have successfully logged out!" });
            }
            return Json(new { Status = "failed", Message = "There was no logged in user." });
        }

        [HttpPost]
        public ActionResult Register(RegisterAccountViewModel model)
        {
            // Check that required fields are entered
            if (!ModelState.IsValid)
            {
                return Json(new { Status = "failed", Message = "Please correct validation errors and resubmit" });
            }

            using (var userStore = new UserStore())
            {
                User user = userStore.GetUserByEmail(model.Email);
                if (user != null)
                {
                    return Json(new { Status = "failed", Message = "An account already exists for that E-mail, to access your account click forgot password from the login page." });
                }

                user = model.UserFactory();
                userStore.Add(user);
                userStore.Save();

                return Json(new { Status = "success", Message = "You have been registered successfully!" });
            }
        }

        [HttpGet]
        public ActionResult LoggedInUser()
        {
            var um = new UserManager();
            if (um.IsCurrentUserLoggedIn())
            {
                var user = um.LoggedInUser;
                return Json(new { Status = "success", User = new UserSummaryViewModel(user), JsonRequestBehavior.AllowGet });
            }
            return Json(new { Status = "failed", Message = "There was no logged in user." }, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //[AutomatedRequest]
        //public ActionResult CheckLogin()
        //{
        //    return Json(new { LoggedIn = (new UserManager().LoggedInUser != null), });
        //}

        //[HttpGet]
        //public ActionResult Login(string returnUrl)
        //{
        //    // If the session user is already logged in go to dashboard
        //    if (new UserManager().IsCurrentUserLoggedIn())
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }

        //    // Setup the view parameters
        //    ViewBag.ReturnUrl = returnUrl;
        //    var email = Request.Cookies["rememberMe"] != null ? Request.Cookies["rememberMe"].Value : "";

        //    // Return the view using an existing cookie
        //    return View(new LoginViewModel()
        //    {
        //        Email = email,
        //        RememberMe = (Request.Cookies["rememberMe"] != null)
        //    });
        //}




        //// TODO: Should be a post request
        //[HttpGet]
        //public ActionResult Logout()
        //{
        //    new UserManager().LogoutUser();
        //    return RedirectToAction("Login");
        //    //return View("/Views/Account/Login");
        //}

        //[HttpGet]
        //public ActionResult ResetPassword(string email, string accessToken, string returnUrl)
        //{
        //    // Check that the token is set
        //    if (accessToken == null || email == null)
        //    {
        //        RedirectToAction("Login");
        //    }
        //    ViewBag.returnUrl = returnUrl;

        //    return View(new ResetPasswordViewModel()
        //    {
        //        Email = email,
        //        AccessToken = accessToken
        //    });
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult ResetPassword(ResetPasswordViewModel model, string returnUrl)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        ModelState.AddModelError("", "Please correct the validation errors and resubmit the form.");
        //        return View(model);
        //    }
        //    using (var userStore = new UserStore())
        //    {
        //        User user = userStore.GetUserByEmail(model.Email);
        //        if (user == null)
        //        {
        //            return RedirectToAction("Login", new { ReturnUrl = returnUrl });
        //        }

        //        var userManager = new UserManager();

        //        if (user.ShouldResetPasswordOnLogin && userManager.CheckPasswords(user.PassHash, model.Password) == PasswordVerificationResult.Success)
        //        {
        //            ModelState.AddModelError("", "Your new password should be different from your old password.");
        //            return View(model);
        //        }

        //        returnUrl = returnUrl ?? Url.Action("Index", "Home");

        //        switch (userManager.AttemptResetPassword(user, model.Password, model.AccessToken))
        //        {
        //            case SignInStatus.Success:
        //                userStore.Save();
        //                return RedirectToLocal(returnUrl);
        //            case SignInStatus.Failure:
        //            default:
        //                ModelState.AddModelError("", "Invalid Reset attempt.");
        //                return View(model);
        //        }
        //    }

        //}

        //[HttpGet]
        //public ActionResult ForgotPassword()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        ModelState.AddModelError("", "Please correct the validation errors and resubmit the form.");
        //        return View(model);
        //    }
        //    using (var userStore = new UserStore())
        //    {
        //        User user = userStore.GetUserByEmail(model.Email);
        //        var userManager = new UserManager();

        //        if (user != null)
        //        {
        //            userManager.GenerateRandomTokenForUser(user);
        //            new AccountEmailManager().SendForgotPasswordEmail(user, userManager.GetResetPasswordLink(user));
        //            userStore.Save();
        //        }

        //        return View(new ForgotPasswordViewModel
        //        {
        //            ShowConfirmationMessage = true
        //        });
        //    }
        //}

        //private ActionResult ForceResetPassword(string returnUrl)
        //{
        //    // Get the logged in user
        //    var userManager = new UserManager();
        //    User user = userManager.LoggedInUser;


        //    if (user != null)
        //    {
        //        using (var userStore = new UserStore())
        //        {
        //            userStore.Attach(user);
        //            userManager.LogoutUser();
        //            // Generate a random access token 
        //            var accessToken = userManager.GenerateRandomTokenForUser(user);

        //            userStore.Save();
        //            // Redirect to the reset password page using the token as a parameter
        //            return RedirectToAction("ResetPassword", new { Email = user.Email, AccessToken = accessToken, ReturnUrl = returnUrl });
        //        }

        //    }
        //    return RedirectToAction("Login");
        //}

        //private ActionResult RedirectToLocal(string returnUrl)
        //{
        //    if (Url.IsLocalUrl(returnUrl))
        //    {
        //        return Redirect(returnUrl);
        //    }
        //    return RedirectToAction("Index", "Home");
        //}
    }
}