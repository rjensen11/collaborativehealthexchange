﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChX.Models.Entitys;
using nextAIMS.Models;

namespace ChX.ViewModels
{
    public class ForumVoteIndexViewModel
    {
        public ForumVoteIndexViewModel(ForumPostVote forumPostVote)
        {
            ID = forumPostVote.ID;
            IsUpvote = forumPostVote.IsUpvote;
            ForumPostID = forumPostVote.ForumPostID;
        }

        public int ID { get; set; }

        public bool IsUpvote { get; set; }

        public int ForumPostID { get; set; }

    }

    public class ForumVoteAddViewModel
    {
        public ForumVoteAddViewModel()
        {
        }

        public ForumPostVote ForumPostVoteFactory()
        {
            var model = new ForumPostVote();
            model.IsUpvote = IsUpvote;
            model.ForumPostID = ForumPostID;
            model.UserID = new UserManager().LoggedInUser.ID;
            return model;
        }

        public bool IsUpvote { get; set; }

        public int ForumPostID { get; set; }

    }
}