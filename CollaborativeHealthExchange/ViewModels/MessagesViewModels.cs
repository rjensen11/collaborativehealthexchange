﻿using ChX.Models.Entitys;
using ChX.Models.Stores;
using nextAIMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace ChX.ViewModels
{
    //public class MessageIndexViewModel
    //{
    //    public MessageIndexViewModel()
    //    {

    //    }

    //    public MessageIndexViewModel(Message model)
    //    {
    //        var loggedInUser = new UserManager().LoggedInUser;
    //        bool isAuthorized = false;
    //        if (model.ReceivingUser.ID == loggedInUser.ID)
    //        {
    //            isAuthorized = true;
    //            LoggedInUserRead = model.ReceivingUserRead;
    //            IsDeleted = model.ReceivingUserDelete;
    //        }
    //        else if (model.SendingUser.ID == loggedInUser.ID)
    //        {
    //            isAuthorized = true;
    //            LoggedInUserRead = model.SendingUserDelete;
    //            IsDeleted = model.SendingUserDelete;
    //        }

    //        ID = model.ID;
    //        if (!isAuthorized)
    //        {
    //            Body = "You do not have the correct access to see this message";
    //            Subject = "Unauthorized Access";
    //        }
    //        else if (IsDeleted)
    //        {
    //            Body = "";
    //            Subject = "";
    //        }
    //        else
    //        {
    //            Body = model.Body;
    //            Subject = model.Subject;
    //            CreationDate = model.CreationDate;
    //            ReceivingUser = new UserSummaryViewModel(model.ReceivingUser);
    //            SendingUser = new UserSummaryViewModel(model.SendingUser);
    //        }
    //        ChildMessageID = model.ChildMessages.FirstOrDefault().ID;
    //    }

    //    public IList<MessageIndexViewModel> CreateMessageThread(Message parentMessage)
    //    {
    //        var list = new List<MessageIndexViewModel>();
    //        var current = parentMessage;
    //        while (current != null)
    //        {
    //            list.Add(new MessageIndexViewModel(current));
    //            current = current.ChildMessages.FirstOrDefault();
    //        }
    //        return list;
    //    }

    //    public int ID { get; set; }

    //    [Required]
    //    public string Body { get; set; }

    //    [StringLength(200)]
    //    public string Subject { get; set; }

    //    public DateTime? CreationDate { get; set; }

    //    public bool LoggedInUserRead { get; set; }

    //    public bool IsDeleted { get; set; }

    //    public int ChildMessageID { get; set; }

    //    public UserSummaryViewModel ReceivingUser { get; set; }

    //    public UserSummaryViewModel SendingUser { get; set; }
    //}

    public class MessageHeaderViewModel
    {
        public MessageHeaderViewModel()
        {

        }
        
        public MessageHeaderViewModel(MessageHeader message, IEnumerable<Message> messages = null)
        {
            ID = message.ID;
            Subject = message.Subject;
            ReceivingUserID = message.ReceivingUserID;
            SendingUserID = message.SendingUserID;
            Messages = messages != null ? messages.Select(m => new MessageDetailViewModel(m)).ToList() : null;
        }

        public int ID { get; set; }

        public string Subject { get; set; }

        public int ReceivingUserID { get; set; }

        public int SendingUserID { get; set; }

        public IEnumerable<MessageDetailViewModel> Messages { get; set; }

    }

    public class MessageDetailViewModel
    {
        public MessageDetailViewModel()
        {

        }

        public MessageDetailViewModel(Message message)
        {
            ID = message.ID;
            Body = message.Body;
            MessageHeaderID = message.MessageHeaderID;
            CreationDate = message.CreationDate;
            SendingUser = new UserSummaryViewModel(message.SendingUser);
            ReceivingUser = new UserLinkViewModel(message.ReceivingUser);
        }

        public int ID { get; set; }

        public string Body { get; set; }

        public int MessageHeaderID { get; set; }

        public DateTime CreationDate { get; set; }

        public UserSummaryViewModel SendingUser { get; set; }

        public UserLinkViewModel ReceivingUser { get; set; }

    }

    public class MessageAddViewModel
    {
        public MessageAddViewModel()
        {

        }

        public MessageHeader MessageFactory()
        {
            var modelHeader = new MessageHeader();
            var model = new Message();
            modelHeader.Messages.Add(model);

            modelHeader.ReceivingUserID = ReceivingUserID.Value;
            modelHeader.SendingUserID = new UserManager().LoggedInUser.ID;
            model.ReceivingUserID = modelHeader.ReceivingUserID;
            model.SendingUserID = modelHeader.SendingUserID;

            modelHeader.Subject = Subject;
            model.Body = Body;
            
            return modelHeader;
        }

        [Required]
        public string Body { get; set; }

        [StringLength(200)]
        public string Subject { get; set; }

        [Required]
        public int? ReceivingUserID { get; set; }

        public int? SendingUserID { get; set; }
    }

    public class MessageReplyViewModel
    {
        public MessageReplyViewModel()
        {

        }

        public Message MessageFactory()
        {
            var model = new Message();
            model.Body = Body;
            model.MessageHeaderID = MessageHeaderID.Value;
            model.ReceivingUserID = ReceivingUserID.Value;
            model.SendingUserID = new UserManager().LoggedInUser.ID;

            return model;
        }

        [Required]
        public string Body { get; set; }

        [Required]
        public int? MessageHeaderID { get; set; }

        [Required]
        public int? ReceivingUserID { get; set; }

        public int? SendingUserID { get; set; }
    }

    //public class MessageUserListViewModel
    //{
    //    public MessageUserListViewModel()
    //    {

    //    }

    //    public MessageUserListViewModel(User user, IEnumerable<Message> messages)
    //    {
    //        User = new UserSummaryViewModel(user);
    //        messages = messages ?? Enumerable.Empty<Message>();
    //        Messages = messages.Select(messageModel => new MessageSummaryViewModel(messageModel));
    //    }

    //    public UserSummaryViewModel User { get; set; }

    //    public IEnumerable<MessageSummaryViewModel> Messages { get; set; }
    //}

    //public class MessageListViewModel
    //{
    //    public MessageListViewModel()
    //    {

    //    }

    //    public MessageListViewModel(IEnumerable<Message> messages)
    //    {
    //        Messages = messages.Select(messageModel => new MessageSummaryViewModel(messageModel));
    //    }

    //    public IEnumerable<MessageSummaryViewModel> Messages { get; set; }
    //}

    public class MessageSummaryViewModel
    {

        public MessageSummaryViewModel()
        {

        }

        public MessageSummaryViewModel(MessageHeader messageHeader)
        {
            var user = new UserManager().LoggedInUser;

            Message message = messageHeader.Messages.Where(m => m.ReceivingUserID == user.ID && !m.ReceivingUserDelete)
                .OrderByDescending(m => m.CreationDate).FirstOrDefault();


            ID = messageHeader.ID;
            Body = message.Body;
            BodySummary = WebUtility.HtmlDecode(Regex.Replace(message.Body, "<[^>]*(>|$)", string.Empty));
            BodySummary = BodySummary.Length <= 200 ? BodySummary : string.Format("{0}...", BodySummary.Substring(0, 200));
            Subject = messageHeader.Subject ?? "No Subject";
            Subject = Subject.Length <= 100 ? Subject : string.Format("{0}...", Subject.Substring(0, 100));
            CreationDate = message.CreationDate;
            ReceivingUserRead = message.ReceivingUserID == messageHeader.ReceivingUserID ? messageHeader.ReceivingUserRead : messageHeader.SendingUserRead;
            SendingUser = new UserLinkViewModel(message.SendingUser);
        }

        public int ID { get; set; }

        public string Body { get; set; }

        [StringLength(200)]
        public string BodySummary { get; set; }

        public string Subject { get; set; }

        public DateTime CreationDate { get; set; }

        public bool ReceivingUserRead { get; set; }

        public UserLinkViewModel SendingUser { get; set; }
    }

    public class MessageNotificationViewModel
    {

        public MessageNotificationViewModel()
        {

        }

        public MessageNotificationViewModel(MessageHeader messageHeader)
        {
            var user = new UserManager().LoggedInUser;

            Message message = messageHeader.Messages.Where(m => m.ReceivingUserID == user.ID && !m.ReceivingUserDelete)
                .OrderByDescending(m => m.CreationDate).FirstOrDefault();

            ID = messageHeader.ID;
            BodySummary = WebUtility.HtmlDecode(Regex.Replace(message.Body, "<[^>]*(>|$)", string.Empty));
            BodySummary = BodySummary.Length <= 200 ? BodySummary : string.Format("{0}...", BodySummary.Substring(0, 200));
            CreationDate = message.CreationDate;
        }

        public int ID { get; set; }

        [StringLength(200)]
        public string BodySummary { get; set; }

        public DateTime CreationDate { get; set; }
    }
}