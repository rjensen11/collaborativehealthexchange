﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChX.Models.Entitys;
using nextAIMS.Models;

namespace ChX.ViewModels
{
    public class CommentVoteIndexViewModel
    {
        public CommentVoteIndexViewModel(CommentVote commentVote)
        {
            ID = commentVote.ID;
            IsUpvote = commentVote.IsUpvote;
            CommentID = commentVote.CommentID;
        }

        public int ID { get; set; }

        public bool IsUpvote { get; set; }

        public int CommentID { get; set; }

    }

    public class CommentVoteAddViewModel
    {
        public CommentVoteAddViewModel()
        {
        }

        public CommentVote CommentVoteFactory()
        {
            var model = new CommentVote();
            model.IsUpvote = IsUpvote;
            model.CommentID = CommentID;
            model.UserID = new UserManager().LoggedInUser.ID;
            return model;
        }

        public bool IsUpvote { get; set; }

        public int CommentID { get; set; }

    }
}