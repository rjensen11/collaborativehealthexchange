﻿using ChX.Models.Entitys;
using nextAIMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace ChX.ViewModels
{
    public class ForumIndexViewModel
    {
        public ForumIndexViewModel(ForumPost model)
        {
            ID = model.ID;
            Body = model.Body;
            Title = model.Title;
            Edited = model.Edited;
            CreationDate = model.CreationDate;
            EditDate = model.EditDate;
            User = new UserSummaryViewModel(model.User);
            var loggedInUser = new UserManager().LoggedInUser;
            var vote = model.ForumPostVotes.Where(v => v.UserID == loggedInUser.ID).FirstOrDefault();
            if (vote != null)
            {
                ForumPostVote = new ForumVoteIndexViewModel(vote);
            }
            ForumPostVotes = model.ForumPostVotes.Select(v => new ForumVoteIndexViewModel(v));
        }

        public ForumIndexViewModel()
        {
        }

        public int ID { get; set; }

        public string Body { get; set; }

        [StringLength(300)]
        public string Title { get; set; }

        public int Views { get; set; }

        public bool Edited { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? EditDate { get; set; }

        public IEnumerable<CommentDetailViewModel> Comments { get; set; }

        public UserSummaryViewModel User { get; set; }

        public ForumVoteIndexViewModel ForumPostVote { get; set; }

        public IEnumerable<ForumVoteIndexViewModel> ForumPostVotes { get; set; }
    }

    public class ForumAddViewModel
    {
        public ForumAddViewModel()
        {

        }

        public ForumAddViewModel(ForumPost model)
        {
            Body = model.Body;
            Title = model.Title;
        }

        public ForumPost ForumPostFactory()
        {
            var model = new ForumPost();
            model.Body = Body;
            model.Title = Title;
            var user = new UserManager().LoggedInUser;
            model.UserID = user.ID;
            return model;
        }

        [Required]
        public string Body { get; set; }
        //[Required]
        //public string Body {
        //    get
        //    {
        //        return BodyHtml != null ? BodyHtml.ToHtmlString() : null;
        //    }
        //    set
        //    {
        //        if (value == null)
        //        {
        //            BodyHtml = null;
        //        }
        //        else
        //        {
        //            BodyHtml = new HtmlString(value);
        //        }
        //    }
        //}

        //public IHtmlString BodyHtml { get; set; }

        [Required]
        [StringLength(300)]
        public string Title { get; set; }
    }

    public class ForumEditViewModel
    {

        public ForumEditViewModel()
        {

        }

        public ForumEditViewModel(ForumPost model)
        {
            Body = model.Body;
            Title = model.Title;
        }

        public ForumPost ForumPostFactory(ForumPost model)
        {
            model.Body = Body;
            model.Title = Title;
            model.Edited = true;
            model.EditDate = DateTime.Now;
            return model;
        }

        public int ID { get; set; }

        [Required]
        public string Body { get; set; }

        [Required]
        [StringLength(300)]
        public string Title { get; set; }

    }

    public class ForumUserListViewModel
    {
        public ForumUserListViewModel()
        {
        }

        public ForumUserListViewModel(User user)
        {
            User = new UserSummaryViewModel(user);
        }

        public UserSummaryViewModel User { get; set; }

        public ForumListViewModel ForumPosts { get; set; }
    }

    public class ForumListViewModel
    {
        public ForumListViewModel()
        {

        }

        public ForumListViewModel(IEnumerable<ForumPost> posts)
        {
            ForumPosts = posts.Select(forumModel => new ForumSummaryViewModel(forumModel));
        }

        public IEnumerable<ForumSummaryViewModel> ForumPosts { get; set; }
    }

    public class ForumSummaryViewModel
    {
        public ForumSummaryViewModel()
        {

        }

        public ForumSummaryViewModel(ForumPost model)
        {
            ID = model.ID;
            Title = model.Title;
            Body = model.Body;
            BodySummary = WebUtility.HtmlDecode(Regex.Replace(model.Body, "<[^>]*(>|$)", string.Empty));
            BodySummary = BodySummary.Length <= 300 ? BodySummary : string.Format("{0}...", BodySummary.Substring(0, 300));
            Views = model.Views;
            CreationDate = model.CreationDate;
            Edited = model.Edited;
            EditDate = model.EditDate;
            Comments = model.Comments.Where(c => !c.Removed).Count() + 1;
            VotesCount = model.ForumPostVotes.Count();
            ScorePercent = (VotesCount == 0) ? 0 : 100 * model.ForumPostVotes.Where(v => v.IsUpvote).Count() / VotesCount;
            User = new UserLinkViewModel(model.User);
        }

        public int ID { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        public string BodySummary { get; set; }

        public int Views { get; set; }

        public DateTime? CreationDate { get; set; }

        public bool Edited { get; set; }

        public DateTime? EditDate { get; set; }

        public int Comments { get; set; }

        public int ScorePercent { get; set; }

        public int VotesCount { get; set; }

        public UserLinkViewModel User { get; set; }
    }

}