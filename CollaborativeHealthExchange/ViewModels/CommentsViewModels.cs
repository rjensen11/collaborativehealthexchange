﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ChX.Models.Entitys;
using nextAIMS.Models;

namespace ChX.ViewModels
{
    public class CommentDetailViewModel
    {
        public CommentDetailViewModel()
        {

        }

        public CommentDetailViewModel(Comment model)
        {
            ID = model.ID;
            Body = model.Body;
            Edited = model.Edited;
            CreationDate = model.CreationDate;
            EditDate = model.EditDate;
            User = new UserSummaryViewModel(model.User);
            var loggedInUser = new UserManager().LoggedInUser;
            var vote = model.CommentVotes.Where(v => v.UserID == loggedInUser.ID).FirstOrDefault();
            if (vote != null)
            {
                CommentVote = new CommentVoteIndexViewModel(vote);
            }
            CommentVotes = model.CommentVotes.Select(v => new CommentVoteIndexViewModel(v));
        }

        public int ID { get; set; }

        [Required]
        public string Body { get; set; }

        public bool Edited { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? EditDate { get; set; }

        public UserSummaryViewModel User { get; set; }

        public CommentVoteIndexViewModel CommentVote { get; set; }

        public IEnumerable<CommentVoteIndexViewModel> CommentVotes { get; set; }
    }

    public class CommentAddViewModel
    {
        public Comment CommentFactory()
        {
            var model = new Comment();
            model.Body = Body;
            model.ForumPostID = ForumPostID;
            var user = new UserManager().LoggedInUser;
            model.UserID = user.ID;
            return model;
        }

        [Required]
        public string Body { get; set; }

        public int ForumPostID { get; set; }

    }

    public class CommentEditViewModel
    {
        public CommentEditViewModel()
        {

        }

        public Comment CommentFactory(Comment model)
        {
            model.Body = Body;
            model.Edited = true;
            model.EditDate = DateTime.Now;
            return model;
        }

        public int ID { get; set; }

        [Required]
        public string Body { get; set; }
    }

    public class CommentUserListViewModel
    {
        public CommentUserListViewModel()
        {

        }

        public CommentUserListViewModel(User user)
        {
            User = new UserSummaryViewModel(user);
            Comments = new CommentListViewModel(user.Comments);
        }

        public UserSummaryViewModel User { get; set; }

        public CommentListViewModel Comments;
    }

    public class CommentListViewModel
    {
        public CommentListViewModel()
        {

        }

        public CommentListViewModel(IEnumerable<Comment> comments)
        {
            Comments = comments.Select(commentModel => new CommentSummaryViewModel(commentModel));
        }

        public IEnumerable<CommentSummaryViewModel> Comments { get; set; }
    }

    public class CommentSummaryViewModel
    {

        public CommentSummaryViewModel(Comment model)
        {
            ID = model.ID;
            // TODO: Parse out any html before substring
            BodySummary = model.Body.Substring(0, 300);
            CreationDate = model.CreationDate;
            Edited = model.Edited;
            EditDate = model.EditDate;
            Score = CommentVote.CalculateScore(model.CommentVotes);
        }

        public int ID { get; set; }

        [Required]
        public string BodySummary { get; set; }

        public DateTime CreationDate { get; set; }

        public bool Edited { get; set; }

        public DateTime? EditDate { get; set; }

        public int Score { get; set; }
    }
}