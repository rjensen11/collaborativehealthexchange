﻿using ChX.Models.Entitys;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace nextAIMS.Models
{

    public class LoginViewModel
    {
        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "The {0} field is required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        public string AccessToken { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterAccountViewModel
    {
        public User UserFactory()
        {
            var model = new User();
            model.Email = Email;
            model.PassHash = new UserManager().HashPassword(Password);
            model.FirstName = Firstname;
            model.LastName = Lastname;
            model.ProfilePicture = ProfilePicture;
            return model;
        }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Firstname")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Lastname")]
        public string Lastname { get; set; }


        [Required(ErrorMessage = "The {0} field is required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string ProfilePicture { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "The {0} field is required")]
        public string Email { get; set; }

        public bool ShowConfirmationMessage { get; set; }
    }

}