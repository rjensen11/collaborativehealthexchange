﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChX.Models.Entitys;
using ChX.Models.Stores;
using nextAIMS.Models;

namespace ChX.ViewModels
{
    //public class UserIndexViewModel
    //{
    //    public UserIndexViewModel()
    //    {

    //    }

    //    public UserIndexViewModel(User user)
    //    {
    //        ID = user.ID;
    //        Email = user.Email;
    //        FirstName = user.FirstName;
    //        LastName = user.LastName;
    //        HasVerifyedEmail = user.HasVerifyedEmail;
    //        DateJoined = user.DateJoined;
    //        AllComments = new CommentListViewModel(user.Comments);
    //        UpvotedComments = new CommentListViewModel(user.CommentVotes.Where(v => v.IsUpvote).Select(commentVote => commentVote.Comment));
    //        DownvotedComments = new CommentListViewModel(user.CommentVotes.Where(v => !v.IsUpvote).Select(commentVote => commentVote.Comment));
    //        SubmittedForumPosts = new ForumListViewModel(user.ForumPosts);
    //        UpvotedForumPosts = new ForumListViewModel(user.ForumPostVotes.Where(v => v.IsUpvote).Select(forumVote => forumVote.ForumPost));
    //        DownvotedForumPosts = new ForumListViewModel(user.ForumPostVotes.Where(v => !v.IsUpvote).Select(forumVote => forumVote.ForumPost));
    //        UnreadMessages = new MessageListViewModel(user.ReceivedMessages.Where(message => !message.ReceivingUserRead && !message.ReceivingUserDelete));
    //        Roles = new RoleListViewModel(user.Roles);
    //    }

    //    public int ID { get; set; }

    //    public string Email { get; set; }

    //    public string FirstName { get; set; }

    //    public string LastName { get; set; }

    //    public bool HasVerifyedEmail { get; set; }

    //    public DateTime? DateJoined { get; set; }

    //    public CommentListViewModel AllComments { get; set; }

    //    public CommentListViewModel UpvotedComments { get; set; }

    //    public CommentListViewModel DownvotedComments { get; set; }

    //    public ForumListViewModel SubmittedForumPosts { get; set; }

    //    public ForumListViewModel UpvotedForumPosts { get; set; }

    //    public ForumListViewModel DownvotedForumPosts { get; set; }

    //    public MessageListViewModel UnreadMessages { get; set; }

    //    public RoleListViewModel Roles { get; set; }
    //}

    //public class UserAddViewModel
    //{
    //    public User AddUser(RoleStore roleStore)
    //    {
    //        var model = new User();
    //        model.Email = Email;
    //        model.PassHash = ShouldGenerateAccessToken ? null : new UserManager().HashPassword(Password);
    //        model.FirstName = FirstName;
    //        model.LastName = LastName;
    //        model.ShouldResetPasswordOnLogin = true;
    //        model.IsSystemAdmin = IsSystemAdmin;
    //        model.Active = Active;

    //        foreach (var id in Roles)
    //        {
    //            model.Roles.Add(roleStore.GetByID(id));
    //        }
    //        return model;
    //    }

    //    [Required(ErrorMessage = "The {0} field is required")]
    //    [Display(Name = "First Name")]
    //    public string FirstName { get; set; }

    //    [Required(ErrorMessage = "The {0} field is required")]
    //    [Display(Name = "Last Name")]
    //    public string LastName { get; set; }

    //    [Required(ErrorMessage = "The {0} field is required")]
    //    [EmailAddress(ErrorMessage = "The {0} field should be an email address")]
    //    [Display(Name = "E-Mail")]
    //    public string Email { get; set; }

    //    [StringLength(150, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
    //    [DataType(DataType.Password)]
    //    [Display(Name = "Temporary Password")]
    //    public string Password { get; set; }

    //    [DataType(DataType.Password)]
    //    [Display(Name = "Confirm password")]
    //    [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
    //    public string ConfirmPassword { get; set; }

    //    [Display(Name = "Generate Single Use Login")]
    //    public bool ShouldGenerateAccessToken { get; set; }

    //    [Required]
    //    [Display(Name = "Active")]
    //    public bool Active { get; set; }

    //    [Display(Name = "System Admin")]
    //    public bool IsSystemAdmin { get; set; }
    //    public IList<int> Roles { get; set; }

    //    public IEnumerable<SelectListItem> RoleChoices { get; set; }

        


    //}

    public class UserEditViewModel
    {
        public UserEditViewModel()
        {

        }

        public UserEditViewModel(User model)
        {
            Email = model.Email;
            FirstName = model.FirstName;
            LastName = model.LastName;
            Active = model.Active;
            ShouldResetPasswordOnLogin = model.ShouldResetPasswordOnLogin;
            TokenValidUntil = model.TokenValidUntil;
            HasVerifyedEmail = model.HasVerifyedEmail;
            IsSystemAdmin = model.IsSystemAdmin;
            Roles = model.Roles.Select(roleModel => roleModel.ID).ToList();
        }

        public User EditUser(User model, RoleStore roleStore)
        {
            model.Email = Email;
            model.FirstName = FirstName;
            model.LastName = LastName;
            model.Active = Active;
            model.ShouldResetPasswordOnLogin = ShouldResetPasswordOnLogin;
            model.TokenValidUntil = TokenValidUntil;
            model.HasVerifyedEmail = HasVerifyedEmail;
            model.IsSystemAdmin = IsSystemAdmin;

            model.Roles.Clear();
            foreach (var id in Roles)
            {
                model.Roles.Add(roleStore.GetByID(id));
            }
            return model;
        }

        public int ID { get; set; }

        [Required]
        [StringLength(300)]
        public string Email { get; set; }

        [Required]
        [StringLength(75)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(75)]
        public string LastName { get; set; }

        public bool Active { get; set; }

        public bool ShouldResetPasswordOnLogin { get; set; }

        public bool ShouldGenerateAccessToken { get; set; }

        public DateTime? TokenValidUntil { get; set; }

        public bool HasVerifyedEmail { get; set; }

        public bool IsSystemAdmin { get; set; }

        public IList<int> Roles { get; set; }

        public IEnumerable<SelectListItem> RoleChoices { get; set; }
    }

    public class UserListViewModel
    {
        public UserListViewModel()
        {

        }

        public UserListViewModel(IEnumerable<User> users)
        {
            Users = users.Select(userModel => new UserSummaryViewModel(userModel));
        }

        public IEnumerable<UserSummaryViewModel> Users { get; set; }
    }

    public class UserSummaryViewModel
    {

        public UserSummaryViewModel()
        {

        }

        public UserSummaryViewModel(User model)
        {
            ID = model.ID;
            Email = model.Email;
            FirstName = model.FirstName;
            LastName = model.LastName;
            //Active = model.Active;
            //HasVerifyedEmail = model.HasVerifyedEmail;
            DateJoined = model.DateJoined;
            ProfilePicture = model.ProfilePicture;

            Reputation = model.CalculateReputation();
            //LastLogin = model.LastLogin;
            //Roles = new RoleListViewModel(model.Roles);
        }

        public int ID { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool Active { get; set; }

        public bool HasVerifyedEmail { get; set; }

        public DateTime? DateJoined { get; set; }

        public DateTime? LastLogin { get; set; }

        public RoleListViewModel Roles { get; set; }

        public long Reputation { get; set; }

        public string ProfilePicture { get; set; }
    }

    public class UserLinkViewModel
    {
        public UserLinkViewModel()
        {

        }

        public UserLinkViewModel(User model)
        {
            ID = model.ID;
            FirstName = model.FirstName;
            LastName = model.LastName;
            Name = string.Format("{0} {1}", model.FirstName, model.LastName);
        }

        public int ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Name { get; set; }

    }
}