﻿using nextAIMS.Models.Entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace nextAIMS.Models
{

    public class UserAddViewModel
    {

        public UserAddViewModel()
        {
            FirstName = "";
            LastName = "";
            ShouldGenerateAccessToken = true;
            Active = true;
        }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        //[DataType(DataType.EmailAddress, ErrorMessage = "The {0} field should be an email address")]
        [EmailAddress(ErrorMessage = "The {0} field should be an email address")]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [StringLength(150, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Temporary Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Generate Single Use Login")]
        public bool ShouldGenerateAccessToken { get; set; }

        [Required]
        [Display(Name = "Active")]
        public bool Active { get; set; }

        [Display(Name = "System Admin")]
        public bool IsSystemAdmin { get; set; }

        [Editable(false)]
        public string LastUsername { get; set; }
    }

    public class UserIndexViewModel
    {
        public IEnumerable<UserSummaryViewModel> users { get; set; }
    }

    public class UserSummaryViewModel
    {
        public UserSummaryViewModel()
        {

        }

        public UserSummaryViewModel(User model)
        {
            GuidId = model.GuidID;
            Name = string.Format("{0} {1}", model.LastName, model.FirstName);
            UserName = model.UserName;
            Active = model.Active ? "Active" : "Inactive";
            Email = model.Email;
        }

        public Guid GuidId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public string Active { get; set; }

    }
}