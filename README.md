# README #

Welcome to the Collaborative Health Exchange - a forum and direct messaging system for patients and doctors. This project was created as a proof of concept for Ryan's senior project in Computer Science Fall 2015. View the full project including presentation, process diagrams, and research document [here](https://drive.google.com/folderview?id=0B2Cf8bY_HH5BcVVsdmNfbldmeEk&usp=sharing).

### How do I get set up? ###

* Download the source code and open/run in Visual Studio
* The database schema is located [here](https://bitbucket.org/rjensen11/collaborativehealth-db)
* The project uses Entity Framework code first from existing database but it could be easily configured to run code first with migrations enabled

### Use guidelines ###

* The project is a sample of my work and can be used or modified freely as long as the [licence](https://bitbucket.org/rjensen11/collaborativehealthexchange/raw/11c93437452e6e90883b5a556ad234238bb2164d/LICENSE.txt) and attribution is maintained in the project

### Who do I talk to? ###

* Ryan Jensen ([rjensen.professional@gmail.com](mailto:rjensen.professional@gmail.com?Subject=CollaborativeHealthExchange))